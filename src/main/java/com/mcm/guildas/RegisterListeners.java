package com.mcm.guildas;

import com.mcm.guildas.inventoryListener.InventoryShop;
import com.mcm.guildas.inventoryListener.InventoryUpgradePlot;
import com.mcm.guildas.inventoryListener.InventoryUpgrades;
import com.mcm.guildas.listeners.*;
import net.citizensnpcs.api.CitizensAPI;
import org.bukkit.Bukkit;

public class RegisterListeners {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new PlayerJoinEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PortalTeleportToBase(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerInteractPlot(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerQuitEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new InventoryUpgrades(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new InventoryShop(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new InventoryUpgradePlot(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new MiningCollectEvent(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerPlaceBlockProtection(), Main.plugin);
        CitizensAPI.registerEvents(new PlayerInteractNpc());
    }
}
