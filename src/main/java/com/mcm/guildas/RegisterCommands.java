package com.mcm.guildas;

import com.mcm.guildas.commands.CommandGuilda;
import com.mcm.guildas.commands.CommandTest;

public class RegisterCommands {

    public static void register() {
        Main.instance.getCommand("guilda").setExecutor(new CommandGuilda());
        Main.instance.getCommand("g").setExecutor(new CommandGuilda());
        Main.instance.getCommand("testando").setExecutor(new CommandTest());
    }
}
