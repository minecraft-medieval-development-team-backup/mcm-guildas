package com.mcm.guildas.inventoryListener;

import com.mcm.core.Main;
import com.mcm.guildas.managers.InventoryGenerator;
import com.mcm.guildas.managers.InventoryStock;
import com.mcm.guildas.managers.InventoryUpgrade;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryUpgrades implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        String uuid = player.getUniqueId().toString();

        if (event.getView().getTitle().equals(Main.getTradution("pC3&Wz%rFVXK%Be", uuid))) {
            event.setCancelled(true);

            if (event.getSlot() == 24) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    player.openInventory(InventoryStock.get(uuid, "elixir_negro"));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 5L);
            }

            if (event.getSlot() == 23) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    player.openInventory(InventoryStock.get(uuid, "elixir"));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 5L);
            }

            if (event.getSlot() == 22) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    player.openInventory(InventoryStock.get(uuid, "coins"));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 5L);
            }

            if (event.getSlot() == 15) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    player.openInventory(InventoryGenerator.get(uuid, "elixir_negro"));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 5L);
            }

            if (event.getSlot() == 14) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    player.openInventory(InventoryGenerator.get(uuid, "elixir"));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 5L);
            }

            if (event.getSlot() == 13) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    player.openInventory(InventoryGenerator.get(uuid, "coins"));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 5L);
            }

            if (event.getSlot() == 11) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    player.openInventory(InventoryUpgrade.getUpgradePlot(uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 5L);
            }

            if (event.getSlot() == 35) {
                player.getOpenInventory().close();
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
    }
}
