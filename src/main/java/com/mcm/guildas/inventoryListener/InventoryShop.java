package com.mcm.guildas.inventoryListener;

import com.mcm.core.Main;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryShop implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        String uuid = player.getUniqueId().toString();

        //INVENTORY PROTECTIONS
        if (event.getView().getTitle().equals(Main.getTradution("ewsGfV&Pnus58np", uuid))) {
            event.setCancelled(true);
        }

        //INVENTORY EXPLOSIVES
        if (event.getView().getTitle().equals(Main.getTradution("#yT*gbWfXP3FNX$", uuid))) {
            event.setCancelled(true);
        }

        //INVENTORY TOOLS
        if (event.getView().getTitle().equals(Main.getTradution("bUg@qb5U&qt6aPA", uuid))) {
            event.setCancelled(true);
        }

        //INVENTORY MENU
        if (event.getView().getTitle().equals(Main.getTradution("b4y9E$nbN!bKCG&", uuid))) {
            event.setCancelled(true);

            if (event.getSlot() == 10) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    player.openInventory(com.mcm.guildas.managers.InventoryShop.getInvTools(uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 5L);
            }

            if (event.getSlot() == 11) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    //player.openInventory(com.mcm.guildas.managers.InventoryShop.getInvWeapons(uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 5L);
            }

            if (event.getSlot() == 12) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    player.openInventory(com.mcm.guildas.managers.InventoryShop.getInvExplosives(uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 5L);
            }

            if (event.getSlot() == 13) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    player.openInventory(com.mcm.guildas.managers.InventoryShop.getInvProtections(uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 5L);
            }

            if (event.getSlot() == 16) {
                Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                    //player.openInventory(com.mcm.guildas.managers.InventoryShop.getInvCosmetics(uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }, 5L);
            }
        }
    }
}
