package com.mcm.guildas.inventoryListener;

import com.mcm.core.Main;
import com.mcm.core.RabbitMq;
import com.mcm.core.database.GuildaDb;
import com.mcm.core.database.RMqChatManager;
import com.mcm.core.enums.ServersList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryUpgradePlot implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        String uuid = player.getUniqueId().toString();

        if (event.getView().getTitle().equals(Main.getTradution("hR5tUv@?z45kSXB", uuid))) {
            event.setCancelled(true);

            if (event.getSlot() == 15) {
                if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1 == 2) {
                    caseCoins(player, 2);
                } else if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1 == 3) {
                    caseCoins(player, 3);
                } else if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1 == 4) {
                    caseCoins(player, 4);
                } else if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1 == 5) {
                    caseElixir(player, 5);
                } else if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1 == 6) {
                    caseElixir(player, 6);
                } else if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1 == 7) {
                    caseElixir(player, 7);
                } else if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1 == 8) {
                    caseElixirNegro(player, 8);
                }
            }

            if (event.getSlot() == 14) {
                player.getOpenInventory().close();
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
    }

    private static void caseElixirNegro(Player player, int level) {
        int price = 0;
        if (level == 8) price = 400000;

        String uuid = player.getUniqueId().toString();
        if (GuildaDb.getelixir_negro(GuildaDb.getGuildaP(uuid)) >= price) {
            if (GuildaDb.getLevel(GuildaDb.getGuildaP(uuid)) >= (12 * GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1)) {
                GuildaDb.updateElixirNegro(GuildaDb.getGuildaP(uuid), GuildaDb.getelixir_negro(GuildaDb.getGuildaP(uuid)) - price);
                GuildaDb.updatePlotLevel(GuildaDb.getGuildaP(uuid));
                player.getOpenInventory().close();

                for (Player target : Bukkit.getOnlinePlayers()) {
                    if (GuildaDb.getGuildaP(target.getUniqueId().toString()).equals(GuildaDb.getGuildaP(uuid))) {
                        target.sendMessage(Main.getTradution("33xUA$gSTbJhmD*", uuid) + GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + " \n ");
                        target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        target.playSound(target.getLocation(), Sound.ITEM_TRIDENT_THUNDER, 1.5f, 1.5f);
                    }
                }

                for (ServersList server : ServersList.values()) if (!server.name().replaceAll("_", "-").equals(Main.server_name)) RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/update_level_plot_guild=" + GuildaDb.getGuildaP(uuid) + "=" + GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + "=elixir_negro=" + price);
            } else sendMsg(player, "YhA!W?K$$w?3Dr#");
        } else sendMsg(player, "dN*EjmCTQ5!Hduy");
    }

    private static void caseElixir(Player player, int level) {
        int price = 0;
        if (level == 5) price = 750000; else if (level == 6) price = 1500000; else if (level == 7) price = 2250000;

        String uuid = player.getUniqueId().toString();
        if (GuildaDb.getElixir(GuildaDb.getGuildaP(uuid)) >= price) {
            if (GuildaDb.getLevel(GuildaDb.getGuildaP(uuid)) >= (12 * GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1)) {
                GuildaDb.updateElixir(GuildaDb.getGuildaP(uuid), GuildaDb.getElixir(GuildaDb.getGuildaP(uuid)) - price);
                GuildaDb.updatePlotLevel(GuildaDb.getGuildaP(uuid));
                player.getOpenInventory().close();

                for (Player target : Bukkit.getOnlinePlayers()) {
                    if (GuildaDb.getGuildaP(target.getUniqueId().toString()).equals(GuildaDb.getGuildaP(uuid))) {
                        target.sendMessage(Main.getTradution("33xUA$gSTbJhmD*", uuid) + GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + " \n ");
                        target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        target.playSound(target.getLocation(), Sound.ITEM_TRIDENT_THUNDER, 1.5f, 1.5f);
                    }
                }

                for (ServersList server : ServersList.values()) if (!server.name().replaceAll("_", "-").equals(Main.server_name)) RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/update_level_plot_guild=" + GuildaDb.getGuildaP(uuid) + "=" + GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + "=elixir=" + price);
            } else sendMsg(player, "YhA!W?K$$w?3Dr#");
        } else sendMsg(player, "f6x7T5!&J4@Kk@H");
    }

    private static void caseCoins(Player player, int level) {
        int price = 0;
        if (level == 2) price = 500000; else if (level == 3) price = 1750000; else if (level == 4) price = 3250000;

        String uuid = player.getUniqueId().toString();
        if (GuildaDb.getCoins(GuildaDb.getGuildaP(uuid)) >= price) {
            int a = GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) - 1;
            if (a == 0) a = 1;
            if (GuildaDb.getLevel(GuildaDb.getGuildaP(uuid)) >= (12 * GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)))) {
                GuildaDb.updateCoins(GuildaDb.getGuildaP(uuid), GuildaDb.getCoins(GuildaDb.getGuildaP(uuid)) - price);
                GuildaDb.updatePlotLevel(GuildaDb.getGuildaP(uuid));
                player.getOpenInventory().close();

                for (Player target : Bukkit.getOnlinePlayers()) {
                    if (GuildaDb.getGuildaP(target.getUniqueId().toString()).equals(GuildaDb.getGuildaP(uuid))) {
                        target.sendMessage(Main.getTradution("33xUA$gSTbJhmD*", uuid) + GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + " \n ");
                        target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        target.playSound(target.getLocation(), Sound.ITEM_TRIDENT_THUNDER, 1.5f, 1.5f);
                    }
                }

                for (ServersList server : ServersList.values()) if (!server.name().replaceAll("_", "-").equals(Main.server_name)) RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/update_level_plot_guild=" + GuildaDb.getGuildaP(uuid) + "=" + GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + "=coins=" + price);
            } else sendMsg(player, "YhA!W?K$$w?3Dr#");
        } else sendMsg(player, "N@8uF4P5BxHd5Jp");
    }

    private static void sendMsg(Player player, String message_id) {
        player.sendMessage(Main.getTradution(message_id, player.getUniqueId().toString()));
        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
    }
}
