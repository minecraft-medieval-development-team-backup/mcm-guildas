package com.mcm.guildas.utils;

import com.mcm.core.database.GuildaDb;
import com.mcm.core.utils.WorldEditUtil;
import com.mcm.guildas.hashs.Plots;
import com.sk89q.worldedit.WorldEditException;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import java.io.File;
import java.io.IOException;

public class TaskSavePlot {

    public static void enable() {
        final Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.currentThread().sleep(60000 * 5); //5 minutes
                } catch (final Exception e) {
                    e.printStackTrace();
                }

                if (Plots.plots != null) for (Integer p : Plots.plots) {
                    if (PlotUtil.spiralCoords(Bukkit.getWorld("world")) != null) {
                        Location location = PlotUtil.spiralCoords(Bukkit.getWorld("world")).get(p - 1);
                        Location pos1 = new Location(location.getWorld(), location.getBlockX() + 50, 63, location.getBlockZ() + 50);
                        Location pos2 = new Location(location.getWorld(), location.getBlockX() - 50, 113, location.getBlockZ() - 50);

                        ArmorStand infos = null;
                        for (Entity entity : location.getNearbyEntities(100, 100, 100)) {
                            if (entity instanceof ArmorStand) {
                                if (entity.getCustomName().contains("plot-info")) infos = (ArmorStand) entity;
                            }
                        }

                        if (infos != null) {
                            String[] split = infos.getCustomName().split(":");
                            try {
                                WorldEditUtil.save("guild-plots/" + split[1], WorldEditUtil.copy(WorldEditUtil.getRegion(pos1, pos2)));
                                GuildaDb.updatePlotSchem(split[1], new File("guild-plots/" + split[1] + ".schem"));
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (WorldEditException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });
        thread.setName("plotsave-thread");
        thread.start();
    }
}
