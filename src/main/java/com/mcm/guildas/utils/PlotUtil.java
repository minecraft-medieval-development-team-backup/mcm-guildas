package com.mcm.guildas.utils;

import com.mcm.core.database.GuildaDb;
import com.mcm.guildas.hashs.Plots;
import net.citizensnpcs.trait.VillagerProfession;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Villager;

import java.util.ArrayList;
import java.util.List;

public class PlotUtil {

    private static int level1 = 12;
    private static int level2 = 17;
    private static int level3 = 22;
    private static int level4 = 27;
    private static int level5 = 32;
    private static int level6 = 37;
    private static int level7 = 43;
    private static int level8 = 50;

    public static boolean isOnArea(final Location loc, final String guild) {
        Location plot = Plots.get(guild).getLocation();

        int size = 0;
        if (GuildaDb.getPlotLevel(guild) == 2) size = level2; else if (GuildaDb.getPlotLevel(guild) == 3) size = level3; else if (GuildaDb.getPlotLevel(guild) == 4) size = level4; else if (GuildaDb.getPlotLevel(guild) == 5) size = level5;
        else if (GuildaDb.getPlotLevel(guild) == 6) size = level6; else if (GuildaDb.getPlotLevel(guild) == 7) size = level7; else if (GuildaDb.getPlotLevel(guild) == 8) size = level8; else size = level1;

        /**
         * Coords of the plot
         */
        Location pos1 = new Location(loc.getWorld(), plot.getBlockX() - size, 63, plot.getBlockZ() - size);
        Location pos2 = new Location(loc.getWorld(), plot.getBlockX() + size, 113, plot.getBlockZ() + size);

        final int p1x = pos1.getBlockX();
        final int p1y = pos1.getBlockY();
        final int p1z = pos1.getBlockZ();
        final int p2x = pos2.getBlockX();
        final int p2y = pos2.getBlockY();
        final int p2z = pos2.getBlockZ();

        final int minX = p1x < p2x ? p1x : p2x;
        final int minY = p1y < p2y ? p1y : p2y;
        final int minZ = p1z < p2z ? p1z : p2z;

        final int maxX = p1x > p2x ? p1x : p2x;
        final int maxY = p1y > p2y ? p1y : p2y;
        final int maxZ = p1z > p2z ? p1z : p2z;

        if ((loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ)) {
            return true;
        }
        return false;
    }

    public static boolean isOnLimits(final Location loc, final String guild) {
        Location plot = Plots.get(guild).getLocation();

        int limits = 200;
        Location pos1 = new Location(loc.getWorld(), plot.getBlockX() - limits, 0, plot.getBlockZ() - limits);
        Location pos2 = new Location(loc.getWorld(), plot.getBlockX() + limits, 256, plot.getBlockZ() + limits);

        final int p1x = pos1.getBlockX();
        final int p1y = pos1.getBlockY();
        final int p1z = pos1.getBlockZ();
        final int p2x = pos2.getBlockX();
        final int p2y = pos2.getBlockY();
        final int p2z = pos2.getBlockZ();

        final int minX = p1x < p2x ? p1x : p2x;
        final int minY = p1y < p2y ? p1y : p2y;
        final int minZ = p1z < p2z ? p1z : p2z;

        final int maxX = p1x > p2x ? p1x : p2x;
        final int maxY = p1y > p2y ? p1y : p2y;
        final int maxZ = p1z > p2z ? p1z : p2z;

        if ((loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ)) {
            return true;
        }
        return false;
    }

    public static Location isNextNpc(final Location loc, final String guild, String npc) {
        Location plot = Plots.get(guild).getLocation();

        int next = 20;
        Location location = null;
        Location pos1 = null;
        Location pos2 = null;
        if (npc.equals("gerador_coins")) {
            location = new Location(loc.getWorld(), plot.getBlockX() + 12, 65, plot.getBlockZ() + 12);
            pos1 = new Location(loc.getWorld(), (plot.getBlockX() + 12) - next, 0, (plot.getBlockZ() + 12) - next);
            pos2 = new Location(loc.getWorld(), (plot.getBlockX() + 12) + next, 256, (plot.getBlockZ() + 12) + next);
        } else if (npc.equals("gerador_elixir")) {
            location = new Location(loc.getWorld(), plot.getBlockX() - 27, 65, plot.getBlockZ() + 27);
            pos1 = new Location(loc.getWorld(), (plot.getBlockX() - 27) - next, 0, (plot.getBlockZ() + 27) - next);
            pos2 = new Location(loc.getWorld(), (plot.getBlockX() - 27) + next, 256, (plot.getBlockZ() + 27) + next);
        } else if (npc.equals("gerador_elixir_negro")) {
            location = new Location(loc.getWorld(), plot.getBlockX() - 42, 65, plot.getBlockZ() - 42);
            pos1 = new Location(loc.getWorld(), (plot.getBlockX() - 42) - next, 0, (plot.getBlockZ() - 42) - next);
            pos2 = new Location(loc.getWorld(), (plot.getBlockX() - 42) + next, 256, (plot.getBlockZ() - 42) + next);
        }

        final int p1x = pos1.getBlockX();
        final int p1y = pos1.getBlockY();
        final int p1z = pos1.getBlockZ();
        final int p2x = pos2.getBlockX();
        final int p2y = pos2.getBlockY();
        final int p2z = pos2.getBlockZ();

        final int minX = p1x < p2x ? p1x : p2x;
        final int minY = p1y < p2y ? p1y : p2y;
        final int minZ = p1z < p2z ? p1z : p2z;

        final int maxX = p1x > p2x ? p1x : p2x;
        final int maxY = p1y > p2y ? p1y : p2y;
        final int maxZ = p1z > p2z ? p1z : p2z;

        if ((loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ)) {
            return location;
        }
        return null;
    }

    public static List<Location> spiralCoords(World world) {
        List<Location> locations = new ArrayList<>();

        int distance = 500;
        int plots = 100;

        for (int x = 1; x <= plots / 10; x++) {
            for (int z = 1; z <= plots / 10; z++) {
                locations.add(new Location(world, 250 + (distance * x), 63, 250 + (distance * z)));
            }
        }
        return locations;
    }
}
