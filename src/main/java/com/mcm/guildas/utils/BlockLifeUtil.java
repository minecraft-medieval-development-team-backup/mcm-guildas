package com.mcm.guildas.utils;

import com.mcm.core.utils.NBTTagUtil;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.Date;

public class BlockLifeUtil {

    private static int lifeBlock1 = 4;
    private static int lifeBlock2 = 3;
    private static int lifeBlock3 = 12;
    private static int lifeBlock4 = 16;

    /**
     * Minutes to regen the block life
     */
    private static int minutesRegen = 1;

    public static String getLife(Block block) {
        if (NBTTagUtil.getStringData(block, "durability") != null) {
            updateLife(block);
            return NBTTagUtil.getStringData(block, "durability");
        } else return null;
    }

    public static String negativeLife(Block block) {
        if (NBTTagUtil.getStringData(block, "durability") != null) {
            updateLife(block);
            NBTTagUtil.setData(block, "durability", String.valueOf(Integer.valueOf(NBTTagUtil.getStringData(block, "durability")) - 1));
            return NBTTagUtil.getStringData(block, "durability");
        } else return null;
    }

    private static void updateLife(Block block) {
        if (NBTTagUtil.getStringData(block, "last_update") == null || NBTTagUtil.getStringData(block, "durability") == null) return;
        try {
            long last_update = Long.parseLong(NBTTagUtil.getStringData(block, "last_update"));
            int durability = Integer.valueOf(NBTTagUtil.getStringData(block, "durability"));
            long difference = (new Date().getTime() - last_update) / 1000;
            int regen = 0;

            if (block.getType().equals(Material.GRAVEL) && durability < lifeBlock1 && difference >= (minutesRegen * 64)) {
                while (difference >= (minutesRegen * 64)) regen++; difference -= 64;
                durability += regen;
                if (durability > lifeBlock1) durability = lifeBlock1;

                NBTTagUtil.setData(block, "last_update", String.valueOf(new Date().getTime()));
                NBTTagUtil.setData(block, "durability", String.valueOf(regen));
            } else if (block.getType().equals(Material.EMERALD_BLOCK) && durability < lifeBlock2 && difference >= (minutesRegen * 64)) {
                while (difference >= (minutesRegen * 64)) regen++; difference -= 64;
                durability += regen;
                if (durability > lifeBlock2) durability = lifeBlock2;

                NBTTagUtil.setData(block, "last_update", String.valueOf(new Date().getTime()));
                NBTTagUtil.setData(block, "durability", String.valueOf(regen));
            } else if (block.getType().equals(Material.OBSIDIAN) && durability < lifeBlock3 && difference >= (minutesRegen * 64)) {
                while (difference >= (minutesRegen * 64)) regen++; difference -= 64;
                durability += regen;
                if (durability > lifeBlock3) durability = lifeBlock3;

                NBTTagUtil.setData(block, "last_update", String.valueOf(new Date().getTime()));
                NBTTagUtil.setData(block, "durability", String.valueOf(regen));
            } else if (block.getType().equals(Material.BEDROCK) && durability < lifeBlock4 && difference >= (minutesRegen * 64)) {
                while (difference >= (minutesRegen * 64)) regen++; difference -= 64;
                durability += regen;
                if (durability > lifeBlock4) durability = lifeBlock4;

                NBTTagUtil.setData(block, "last_update", String.valueOf(new Date().getTime()));
                NBTTagUtil.setData(block, "durability", String.valueOf(regen));
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
}
