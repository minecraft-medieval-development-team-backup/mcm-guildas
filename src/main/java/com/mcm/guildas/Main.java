package com.mcm.guildas;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static Main instance;
    public static Plugin plugin;

    public static Main getInstance() {
        return Main.instance;
    }

    @Override
    public void onEnable() {
        Main.instance = this;
        Main.plugin = this;
        RegisterCommands.register();
        RegisterListeners.register();
        RegisterMain.register();
    }

    @Override
    public void onDisable() {
        RegisterDisable.register();
    }
}
