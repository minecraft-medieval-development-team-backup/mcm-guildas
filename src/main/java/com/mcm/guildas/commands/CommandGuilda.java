package com.mcm.guildas.commands;

import com.mcm.core.Main;
import com.mcm.core.RabbitMq;
import com.mcm.core.cache.GuildaData;
import com.mcm.core.cache.InvitesGuilda;
import com.mcm.core.cache.OnlineCount;
import com.mcm.core.cache.PlayerLocation;
import com.mcm.core.database.CoinsDb;
import com.mcm.core.database.GuildaDb;
import com.mcm.core.database.RMqChatManager;
import com.mcm.core.enums.GuildsServers;
import com.mcm.core.enums.ServersList;
import com.mcm.guildas.hashs.TryTeleport;
import com.mcm.guildas.listeners.PortalTeleportToBase;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;
import java.util.Arrays;

public class CommandGuilda implements CommandExecutor {

    public static DecimalFormat formatter = new DecimalFormat("#,###.00");

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();

        // /guilda - Abrirá um inv com informações importantes da guildas.
        // /guilda criar (nome) - Será necessário desembolsar X.XXX,XX$, logo que criado o usuário poderá invitar seus amigos para o mesmo.
        // /guilda invitar (nick) - Convidara o respectivo usuário a se juntar a guilda.
        // /guilda promover (nick) - Promovera a um cargo maior.
        // /guilda rebaixar (nick) - Rebaixara o usuário em questão.
        // /guilda kickar (nick) - Kickara o usuário da guilda.
        // /guilda aceitar (name) - Aceitara entrar na guilda.
        // /guilda sair - Saira da guilda
        // /guilda deletar - Deletara a guilda
        // /g (msg) - Chat da guilda
        if (command.getName().equalsIgnoreCase("guilda")) {
            if (args.length == 0) {
                //-
                if (GuildaDb.getGuildaP(player.getUniqueId().toString()) != null) {
                    if (GuildaDb.getAlocated(GuildaDb.getGuildaP(player.getUniqueId().toString())) != null) {
                        if (TryTeleport.get(player.getUniqueId().toString()) == null) {
                            new TryTeleport(player.getUniqueId().toString()).insert();
                            Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
                                TryTeleport.get(player.getUniqueId().toString()).delete();
                            }, 100L);

                            PortalTeleportToBase.connect(player, player.getUniqueId().toString(), GuildaDb.getAlocated(GuildaDb.getGuildaP(player.getUniqueId().toString())));
                        }
                    }
                    for (GuildsServers server : GuildsServers.values()) {
                        if (OnlineCount.get(server.name().replaceAll("_", "-")) != null && OnlineCount.get(server.name().replaceAll("_", "-")).getCount() < 80) {
                            if (TryTeleport.get(player.getUniqueId().toString()) == null) {
                                new TryTeleport(player.getUniqueId().toString()).insert();
                                Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
                                    TryTeleport.get(player.getUniqueId().toString()).delete();
                                }, 100L);

                                PortalTeleportToBase.connect(player, player.getUniqueId().toString(), server.name().replaceAll("_", "-"));
                            }
                        }
                    }
                }
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("sair")) {
                    if (GuildaDb.getGuildaP(uuid) != null) {
                        if (!GuildaDb.getRole(uuid).equals("ceo")) {
                            String guild = GuildaDb.getGuildaP(uuid);
                            String update = null;
                            for (String member : GuildaDb.getMembros(guild).split(":")) {
                                if (!member.contains(uuid)) if (update == null) update = member; else update += ":" + member;
                            }
                            GuildaDb.updateMembros(guild, update);
                            GuildaDb.removeGuildaP(uuid);

                            player.sendMessage(Main.getTradution("WrsSZyy7QJ&YHAk", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);

                            for (Player target : Bukkit.getOnlinePlayers()) {
                                if (!target.getName().equals(player.getName()) && GuildaDb.getGuildaP(target.getUniqueId().toString()).equals(args[1])) {
                                    target.sendMessage(ChatColor.GREEN + " \n * " + player.getName() + Main.getTradution("Vf!$nk*SFBHs$B5", target.getUniqueId().toString()));
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            }

                            for (ServersList server : ServersList.values()) {
                                if (!server.name().replaceAll("_", "-").equals(Main.server_name))
                                    RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/user_exited_guild=" + guild + "=" + player.getName() + "=" + update);
                            }
                        } else sendMsg(player, "gMmvS$6?mfS69ny");
                    } else sendMsg(player, "p9x3e!$9qzSSA53");
                } else if (args[0].equalsIgnoreCase("deletar")) {
                    //-
                } else sendMsg(player, "");
            } else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("promover")) {
                    //-
                } else if (args[0].equalsIgnoreCase("rebaixar")) {
                    //-
                } else  if (args[0].equalsIgnoreCase("criar")) {
                    int price = 20000;
                    if (CoinsDb.getCoins(uuid) >= price) {
                        if (GuildaDb.getGuildaP(uuid) == null) {
                            if (args[1].length() <= 10) {
                                if (GuildaDb.getMembros(args[1]) == null) {
                                    CoinsDb.updateCoins(uuid, CoinsDb.getCoins(uuid) - price);
                                    GuildaDb.setGuilda(args[1], uuid);

                                    player.sendMessage(Main.getTradution("4KH!2dpfdy&!fUX", player.getUniqueId().toString()) + Main.getTradution("NnPFRK%h8m5#hX%", player.getUniqueId().toString()) + args[1] + "\n" + Main.getTradution("7A9WZKSdhwf6#cv", player.getUniqueId().toString()) + formatter.format(price) + "\n");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                                } else sendMsg(player, "&sP!*$b2%DGfF%N");
                            } else sendMsg(player, "*yX@Vv2vkxUh!GX");
                        } else sendMsg(player, "Xjvs8a#G**TMAsW");
                    } else sendMsg(player, "b3qUyt2@%F4N!hq");
                } else if (args[0].equalsIgnoreCase("invitar")) {
                    if (GuildaDb.getGuildaP(uuid) != null) {
                        String[] perm = {"ceo", "admin"};
                        if (Arrays.asList(perm).contains(GuildaDb.getRole(uuid))) {
                            String server = PlayerLocation.get(args[1]);
                            if (server != null) {
                                RabbitMq.send(server, RMqChatManager.key + "/invite_guild=" + GuildaDb.getGuildaP(uuid) + "=" + args[1]);
                            } else sendMsg(player, "6MkuTqdhcWpn*D4");
                        } else sendMsg(player, "M5msgCyf!z@S5NR");
                    } else sendMsg(player, "p9x3e!$9qzSSA53");
                } else if (args[0].equalsIgnoreCase("kickar")) {
                    if (GuildaDb.getGuildaP(uuid) == null) {
                        String[] perm = {"admin", "ceo"};
                        if (Arrays.asList(perm).contains(GuildaDb.getRole(uuid))) {
                            if (!args[1].equalsIgnoreCase(player.getName())) {
                                if (GuildaDb.getMembros(GuildaDb.getGuildaP(uuid)).contains(args[1])) {
                                    String uuid_target = null;
                                    String update = null;
                                    for (String split : GuildaDb.getMembros(GuildaDb.getGuildaP(uuid)).split(":")) {
                                        if (split.contains(args[1])) {
                                            uuid_target = split.split(args[1] + "#")[1];
                                        } else {
                                            if (update == null) update = split;
                                            else update += ":" + split;
                                        }
                                    }
                                    if (!Arrays.asList(perm).contains(GuildaDb.getRole(uuid_target))) {
                                        kick(player, uuid, update, args);
                                    } else if (GuildaDb.getRole(uuid).equals("ceo")) {
                                        kick(player, uuid, update, args);
                                    } else sendMsg(player, "M5msgCyf!z@S5NR");
                                } else sendMsg(player, "8YfVrK6MGA@d7qR");
                            } else sendMsg(player, "?N6KRxsp*2532jF");
                        } else sendMsg(player, "M5msgCyf!z@S5NR");
                    } else sendMsg(player, "p9x3e!$9qzSSA53");
                } else if (args[0].equalsIgnoreCase("aceitar")) {
                    if (GuildaDb.getGuildaP(uuid) == null) {
                        if (InvitesGuilda.get(uuid) != null && InvitesGuilda.get(uuid).getList().contains(args[1])) {
                            GuildaDb.setGuildaP(uuid, args[1], "membro");
                            String update = null;
                            if (GuildaDb.getMembros(args[1]) != null)
                                update = GuildaDb.getMembros(args[1]) + ":" + player.getName() + "#" + uuid;
                            else update = player.getName() + "#" + uuid;
                            GuildaDb.updateMembros(args[1], update);

                            player.sendMessage(Main.getTradution("kG&htF##dgFN7n9", uuid) + args[1] + ".");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);

                            for (Player target : Bukkit.getOnlinePlayers()) {
                                if (!target.getName().equals(player.getName()) && GuildaDb.getGuildaP(target.getUniqueId().toString()).equals(args[1])) {
                                    target.sendMessage(ChatColor.GREEN + " \n * " + player.getName() + Main.getTradution("4PKcZCn2?QRr*WR", target.getUniqueId().toString()));
                                    target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                }
                            }

                            for (ServersList server : ServersList.values()) {
                                if (!server.name().replaceAll("_", "-").equals(Main.server_name))
                                    RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/new_user_guild=" + args[1] + "=" + update + "=" + player.getName());
                            }
                        } else sendMsg(player, "2#w2CGZ9aqN8*gJ");
                    } else sendMsg(player, "p9x3e!$9qzSSA53");
                } else sendMsg(player, "");
            } else sendMsg(player, "");
        }
        return false;
    }

    private static void sendMsg(Player player, String message_id) {
        player.sendMessage(Main.getTradution(message_id, player.getUniqueId().toString()));
        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
    }

    private static void kick(Player player, String uuid, String update, String[] args) {
        GuildaDb.updateMembros(GuildaDb.getGuildaP(uuid), update);

        for (Player target : Bukkit.getOnlinePlayers()) {
            if (GuildaDb.getGuildaP(target.getUniqueId().toString()).equals(args[1])) {
                target.sendMessage(ChatColor.GREEN + " \n * " + args[1] + Main.getTradution("94px?3BjrXyCTS?", target.getUniqueId().toString()));
                target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }

        Player target = Bukkit.getPlayerExact(args[1]);
        if (target != null && target.isOnline()) {
            target.sendMessage(Main.getTradution("Y3D$rA&!NVjKEv@", target.getUniqueId().toString()));
            target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            target.playSound(target.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }

        for (ServersList server : ServersList.values()) {
            if (!server.name().replaceAll("_", "-").equals(Main.server_name))
                RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/user_removed_guild=" + GuildaDb.getGuildaP(uuid) + "=" + args[1] + "=" + update + "=" + player.getName());
        }
    }
}
