package com.mcm.guildas.commands;

import com.mcm.core.utils.NBTTagUtil;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Date;

public class CommandTest implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();

        if (command.getName().equalsIgnoreCase("testando")) {
            ItemStack obsidian = new ItemStack(Material.OBSIDIAN, 64);
            obsidian = NBTTagUtil.setData(obsidian, "type", "protection-3");
            player.getInventory().addItem(obsidian);
            player.sendMessage("obsidian cedida");
            player.updateInventory();
        }
        return false;
    }
}
