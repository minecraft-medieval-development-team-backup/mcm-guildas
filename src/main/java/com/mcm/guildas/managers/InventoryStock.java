package com.mcm.guildas.managers;

import com.mcm.core.Main;
import com.mcm.core.database.GuildaDb;
import com.mcm.core.utils.EnchantmentGlow;
import com.mcm.core.utils.RemoveAllFlags;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class InventoryStock {

    public static DecimalFormat formatter = new DecimalFormat("#,###.00");

    public static Inventory get(String uuid, String type) {
        String inv_name = null;
        if (type.equals("coins")) inv_name = Main.getTradution("6EGyz8t&DjX$qhU", uuid);
        else if (type.equals("elixir")) inv_name = Main.getTradution("?vvY2rgpu$xx92y", uuid);
        else if (type.equals("elixir_negro")) inv_name = Main.getTradution("KwNdM8#4e3UDBa5", uuid);
        Inventory inventory = Bukkit.createInventory(null, 6 * 9, inv_name);

        //Limites = Coins 5.000.000 > Elixir 2.500.000 > Petroleo 500.000
        ItemStack item = new ItemStack(Material.BOOK);
        ItemMeta meta = item.getItemMeta();
        if (type.equals("coins")) meta.setDisplayName(Main.getTradution("@Wbw2XYChhr5J*Z", uuid));
        else if (type.equals("elixir")) meta.setDisplayName(Main.getTradution("ebeyA&3f7GXGQ%V", uuid));
        else if (type.equals("elixir_negro")) meta.setDisplayName(Main.getTradution("xeVaDgXH?F5R7Ka", uuid));
        ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        //Coins stock level
        if (type.equals("coins")) lore.add(Main.getTradution("%bUPjvmFMyvvy9X", uuid) + ChatColor.GREEN + GuildaDb.getCoinsStockLevel(GuildaDb.getGuildaP(uuid)));
        else if (type.equals("elixir")) lore.add(Main.getTradution("%bUPjvmFMyvvy9X", uuid) + ChatColor.GREEN + GuildaDb.getElixirStockLevel(GuildaDb.getGuildaP(uuid)));
        else if (type.equals("elixir_negro")) lore.add(Main.getTradution("%bUPjvmFMyvvy9X", uuid) + ChatColor.GREEN + GuildaDb.getelixir_negroStockLevel(GuildaDb.getGuildaP(uuid)));
        //Quantida limite atual
        if (type.equals("coins")) lore.add(Main.getTradution("rTAhk$9tG#tXx36", uuid) + ChatColor.GREEN + GuildaDb.getCoinsStockLevel(GuildaDb.getGuildaP(uuid)) * 250000);
        else if (type.equals("elixir")) lore.add(Main.getTradution("rTAhk$9tG#tXx36", uuid) + ChatColor.GREEN + GuildaDb.getElixirStockLevel(GuildaDb.getGuildaP(uuid)) * 125000);
        else if (type.equals("elixir_negro")) lore.add(Main.getTradution("rTAhk$9tG#tXx36", uuid) + ChatColor.GREEN + GuildaDb.getelixir_negroStockLevel(GuildaDb.getGuildaP(uuid)) * 25000);
        lore.add(" ");
        //Próximo nível
        if (type.equals("coins")) lore.add(Main.getTradution("pMEsq#D%QPW2p*8", uuid) + ChatColor.GOLD + (GuildaDb.getCoinsStockLevel(GuildaDb.getGuildaP(uuid)) + 1));
        else if (type.equals("elixir")) lore.add(Main.getTradution("pMEsq#D%QPW2p*8", uuid) + ChatColor.GOLD + (GuildaDb.getElixirStockLevel(GuildaDb.getGuildaP(uuid)) + 1));
        else if (type.equals("elixir_negro")) lore.add(Main.getTradution("pMEsq#D%QPW2p*8", uuid) + ChatColor.GOLD + (GuildaDb.getelixir_negroStockLevel(GuildaDb.getGuildaP(uuid)) + 1));
        //Liberara
        if (type.equals("coins")) lore.add(Main.getTradution("8Ma8Ezk#Tag@V5R", uuid) + ChatColor.GOLD + (GuildaDb.getCoinsStockLevel(GuildaDb.getGuildaP(uuid)) + 1) * 250000);
        else if (type.equals("elixir")) lore.add(Main.getTradution("8Ma8Ezk#Tag@V5R", uuid) + ChatColor.GOLD + (GuildaDb.getElixirStockLevel(GuildaDb.getGuildaP(uuid)) + 1) * 125000);
        else if (type.equals("elixir_negro")) lore.add(Main.getTradution("8Ma8Ezk#Tag@V5R", uuid) + ChatColor.GOLD + (GuildaDb.getelixir_negroStockLevel(GuildaDb.getGuildaP(uuid)) + 1) * 25000);
        //Valor do upgrade
        if (type.equals("coins")) lore.add(Main.getTradution("yyJMZ7Jf$dkVFVX", uuid) + ChatColor.GOLD + formatter.format(((GuildaDb.getCoinsStockLevel(GuildaDb.getGuildaP(uuid)) * 250000) / 100) * 75));
        else if (type.equals("elixir")) lore.add(Main.getTradution("yyJMZ7Jf$dkVFVX", uuid) + ChatColor.GOLD + formatter.format(((GuildaDb.getElixirStockLevel(GuildaDb.getGuildaP(uuid)) * 125000) / 100) * 75));
        else if (type.equals("elixir_negro")) lore.add(Main.getTradution("yyJMZ7Jf$dkVFVX", uuid) + ChatColor.GOLD + formatter.format(((GuildaDb.getelixir_negroStockLevel(GuildaDb.getGuildaP(uuid)) * 25000) / 100) * 75));
        //Nível necessario
        if (type.equals("coins")) lore.add(Main.getTradution("nU9FG@#P3wBBr*g", uuid) + ChatColor.GOLD + 5 * GuildaDb.getCoinsStockLevel(GuildaDb.getGuildaP(uuid)));
        else if (type.equals("elixir")) lore.add(Main.getTradution("nU9FG@#P3wBBr*g", uuid) + ChatColor.GOLD + 5 * GuildaDb.getElixirStockLevel(GuildaDb.getGuildaP(uuid)));
        else if (type.equals("elixir_negro")) lore.add(Main.getTradution("nU9FG@#P3wBBr*g", uuid) + ChatColor.GOLD + 5 * GuildaDb.getelixir_negroStockLevel(GuildaDb.getGuildaP(uuid)));
        lore.add(" ");
        lore.add(Main.getTradution("*@M**qJ8h3t2R9g", uuid));
        meta.setLore(lore);
        item.setItemMeta(meta);

        inventory.setItem(10, RemoveAllFlags.remove(item));

        int moeda = 0;
        if (type.equals("coins")) moeda = GuildaDb.getCoinsStockLevel(GuildaDb.getGuildaP(uuid)); else if (type.equals("elixir")) moeda = GuildaDb.getElixirStockLevel(GuildaDb.getGuildaP(uuid)); else if (type.equals("elixir_negro")) moeda = GuildaDb.getelixir_negroStockLevel(GuildaDb.getGuildaP(uuid));
        int n = 1;
        int[] slots = {12, 13, 14, 15, 16, 21, 22, 23, 24, 25, 30, 31, 32, 33, 34, 39, 40, 41, 42, 43};
        for (int i = 1; i <= slots.length; i++) {
            if (n > moeda) {
                ItemStack locked = new ItemStack(Material.RED_STAINED_GLASS);
                ItemMeta metaLocked = locked.getItemMeta();
                metaLocked.setDisplayName(Main.getTradution("dDR5yt7ZZJtzp%G", uuid) + n);
                ArrayList<String> loreLocked = new ArrayList<>();
                loreLocked.add(Main.getTradution("QxD2Hq&HqqX7Rb$", uuid));
                if (moeda + 1 == n) {
                    loreLocked.add(" ");
                    loreLocked.add(Main.getTradution("*@M**qJ8h3t2R9g", uuid));
                }
                metaLocked.setLore(loreLocked);
                locked.setItemMeta(metaLocked);
                inventory.setItem(slots[n - 1], RemoveAllFlags.remove(locked));
            } else {
                ItemStack unlocked = new ItemStack(Material.LIME_STAINED_GLASS);
                ItemMeta metaUnlo = unlocked.getItemMeta();
                metaUnlo.setDisplayName(Main.getTradution("dDR5yt7ZZJtzp%G", uuid) + n);
                ArrayList<String> loreUnlo = new ArrayList<>();
                loreUnlo.add(Main.getTradution("zq2577&mYSReJ#Q", uuid));
                metaUnlo.setLore(loreUnlo);
                unlocked.setItemMeta(metaUnlo);
                inventory.setItem(slots[n - 1], EnchantmentGlow.addGlow(RemoveAllFlags.remove(unlocked)));
            }
            n++;
        }

        return inventory;
    }
}
