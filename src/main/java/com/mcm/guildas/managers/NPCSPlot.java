package com.mcm.guildas.managers;

import com.mcm.core.Main;
import com.mcm.guildas.hashs.Plots;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;
import net.citizensnpcs.trait.LookClose;
import net.citizensnpcs.trait.VillagerProfession;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;

import java.util.ArrayList;

public class NPCSPlot {

    public static void disableAllNpc() {
        for (NPCRegistry registry : CitizensAPI.getNPCRegistries()) {
            registry.deregisterAll();
        }
    }

    public static void disable(String guild) {
        if (CitizensAPI.getNPCRegistry().getById(Plots.get(guild).getNpc_instrutor()) != null)
            CitizensAPI.getNPCRegistry().getById(Plots.get(guild).getNpc_instrutor()).destroy();
        if (CitizensAPI.getNPCRegistry().getById(Plots.get(guild).getNpc_mercador()) != null)
            CitizensAPI.getNPCRegistry().getById(Plots.get(guild).getNpc_mercador()).destroy();
        if (CitizensAPI.getNPCRegistry().getById(Plots.get(guild).getNpc_evolucao()) != null)
            CitizensAPI.getNPCRegistry().getById(Plots.get(guild).getNpc_evolucao()).destroy();
    }

    public static void enable(Location location, String guild, String uuid) {
        if (Plots.npcs == null) Plots.npcs = new ArrayList<>();
        //Instrutor
        NPC instrutor = CitizensAPI.getNPCRegistry().createNPC(EntityType.PLAYER, Main.getTradution("4Fx6tmAMr?NfMXt", uuid));
        instrutor.data().set(NPC.PLAYER_SKIN_TEXTURE_PROPERTIES_SIGN_METADATA, "");
        instrutor.data().set(NPC.PLAYER_SKIN_TEXTURE_PROPERTIES_METADATA, "");
        instrutor.setProtected(true);
        instrutor.spawn(new Location(location.getWorld(), location.getBlockX(), 65, location.getBlockZ() + 71));
        instrutor.addTrait(LookClose.class);
        instrutor.getTrait(LookClose.class).lookClose(true);
        Plots.get(guild).setNpc_instrutor(instrutor.getId());
        Plots.npcs.add(instrutor);

        //Mercador
        NPC mercador = CitizensAPI.getNPCRegistry().createNPC(EntityType.VILLAGER, Main.getTradution("S!8NRzr&UCWmEby", uuid));
        //vilMer.setVillagerType(Villager.Type.SWAMP);
        mercador.addTrait(VillagerProfession.class);
        mercador.getTrait(VillagerProfession.class).setProfession(Villager.Profession.FLETCHER);
        mercador.addTrait(LookClose.class);
        mercador.getTrait(LookClose.class).lookClose(true);
        mercador.setProtected(true);
        mercador.spawn(new Location(location.getWorld(), location.getBlockX() + 5, 65, location.getBlockZ() + 68));
        Plots.get(guild).setNpc_mercador(mercador.getId());
        Plots.npcs.add(mercador);

        //Npc evolução plot
        NPC evolucao_plot = CitizensAPI.getNPCRegistry().createNPC(EntityType.VILLAGER, ChatColor.YELLOW + "Mestre das evoluções");
        //vilEvol.setVillagerType(Villager.Type.SNOW);
        evolucao_plot.addTrait(VillagerProfession.class);
        evolucao_plot.getTrait(VillagerProfession.class).setProfession(Villager.Profession.WEAPONSMITH);
        evolucao_plot.addTrait(LookClose.class);
        evolucao_plot.getTrait(LookClose.class).lookClose(true);
        evolucao_plot.setProtected(true);
        evolucao_plot.spawn(new Location(location.getWorld(), location.getBlockX() - 5, 65, location.getBlockZ() + 68));
        Plots.get(guild).setNpc_evolucao(evolucao_plot.getId());
        Plots.npcs.add(evolucao_plot);

        /*
        //Npc gerador coins
        //vilGerCoi.setVillagerType(Villager.Type.TAIGA);
        gerador_coins.getTrait(VillagerProfession.class).setProfession(Villager.Profession.ARMORER);
        gerador_coins.spawn(new Location(location.getWorld(), location.getBlockX() + 12, 65, location.getBlockZ() + 12));

        //Npc gerador elixir
        if (GuildaDb.getPlotLevel(guild) >= 4) {
            //vilGerEli.setVillagerType(Villager.Type.SAVANNA);
            gerador_elixir.getTrait(VillagerProfession.class).setProfession(Villager.Profession.ARMORER);
            gerador_elixir.spawn(new Location(location.getWorld(), location.getBlockX() - 27, 65, location.getBlockZ() + 27));
        }

        //Npc gerador elixir negro
        if (GuildaDb.getPlotLevel(guild) >= 7) {
            //vilGerEliN.setVillagerType(Villager.Type.SWAMP);
            gerador_elixir_negro.getTrait(VillagerProfession.class).setProfession(Villager.Profession.ARMORER);
            gerador_elixir_negro.spawn(new Location(location.getWorld(), location.getBlockX() - 42, 65, location.getBlockZ() - 42));
        }

        //Npc estoque coins
        //vilEstCoi.setVillagerType(Villager.Type.JUNGLE);
        estoque_coins.getTrait(VillagerProfession.class).setProfession(Villager.Profession.LIBRARIAN);
        estoque_coins.spawn(new Location(location.getWorld(), location.getBlockX() + 71, 65, location.getBlockZ()));

        //Npc estoque elixir
        if (GuildaDb.getPlotLevel(guild) >= 4) {
            //vilEstEli.setVillagerType(Villager.Type.SAVANNA);
            estoque_elixir.getTrait(VillagerProfession.class).setProfession(Villager.Profession.LIBRARIAN);
            estoque_elixir.spawn(new Location(location.getWorld(), location.getBlockX() - 71, 65, location.getBlockZ()));
        }

        //Npc estoque elixir negro
        if (GuildaDb.getPlotLevel(guild) >= 7) {
            //vilEstEliN.setVillagerType(Villager.Type.SWAMP);
            estoque_elixir_negro.getTrait(VillagerProfession.class).setProfession(Villager.Profession.LIBRARIAN);
            estoque_elixir_negro.spawn(new Location(location.getWorld(), location.getBlockX(), 65, location.getBlockZ() - 71));
        }
         */
    }
}
