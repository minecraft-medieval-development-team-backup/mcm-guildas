package com.mcm.guildas.managers;

import com.mcm.core.Main;
import com.mcm.core.database.GuildaDb;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import java.util.ArrayList;

public class InventoryUpgrade {

    public static Inventory get(String uuid) {
        Inventory inventory = Bukkit.createInventory(null, 4 * 9, Main.getTradution("pC3&Wz%rFVXK%Be", uuid));

        ItemStack plot = new ItemStack(Material.STONE);
        ItemMeta metaPlot = plot.getItemMeta();
        metaPlot.setDisplayName(Main.getTradution("VFwWDqS7g#@#4Zp", uuid));
        ArrayList<String> lorePlot = new ArrayList<>();
        lorePlot.add(" ");
        lorePlot.add(Main.getTradution("4s2F*sm@$Dnv9eZ", uuid));
        metaPlot.setLore(lorePlot);
        plot.setItemMeta(metaPlot);

        ItemStack coinsStock = new ItemStack(Material.GOLD_BLOCK);
        ItemMeta metaCoinsS = coinsStock.getItemMeta();
        metaCoinsS.setDisplayName(Main.getTradution("UG&t$yFKr7avb5$", uuid));
        ArrayList<String> loreCoinsS = new ArrayList<>();
        loreCoinsS.add(" ");
        loreCoinsS.add(Main.getTradution("4s2F*sm@$Dnv9eZ", uuid));
        metaCoinsS.setLore(loreCoinsS);
        coinsStock.setItemMeta(metaCoinsS);

        ItemStack elixirS = new ItemStack(Material.REDSTONE_BLOCK);
        ItemMeta metaElixirS = elixirS.getItemMeta();
        metaElixirS.setDisplayName(Main.getTradution("9UgXvQTTY@M*q86", uuid));
        ArrayList<String> loreElixirS = new ArrayList<>();
        loreElixirS.add(" ");
        loreElixirS.add(Main.getTradution("4s2F*sm@$Dnv9eZ", uuid));
        metaElixirS.setLore(loreElixirS);
        elixirS.setItemMeta(metaElixirS);

        ItemStack elixirNS = new ItemStack(Material.COAL_BLOCK);
        ItemMeta metaElixirNS = elixirNS.getItemMeta();
        metaElixirNS.setDisplayName(Main.getTradution("Yr2JPjFXg*#W&d!", uuid));
        ArrayList<String> loreElixirNS = new ArrayList<>();
        loreElixirNS.add(" ");
        loreElixirNS.add(Main.getTradution("4s2F*sm@$Dnv9eZ", uuid));
        metaElixirNS.setLore(loreElixirNS);
        elixirNS.setItemMeta(metaElixirNS);

        ItemStack coins = new ItemStack(Material.SUNFLOWER);
        ItemMeta metaCoins = coins.getItemMeta();
        metaCoins.setDisplayName(Main.getTradution("$B?WV!qCXEqM7D@", uuid));
        ArrayList<String> loreCoins = new ArrayList<>();
        loreCoins.add(" ");
        loreCoins.add(Main.getTradution("4s2F*sm@$Dnv9eZ", uuid));
        metaCoins.setLore(loreCoins);
        coins.setItemMeta(metaCoins);

        ItemStack elixir = new ItemStack(Material.PURPLE_DYE);
        ItemMeta metaElixir = elixir.getItemMeta();
        metaElixir.setDisplayName(Main.getTradution("Wj582TDzwNxXX@h", uuid));
        ArrayList<String> loreElixir = new ArrayList<>();
        loreElixir.add(" ");
        loreElixir.add(Main.getTradution("4s2F*sm@$Dnv9eZ", uuid));
        metaElixir.setLore(loreElixir);
        elixir.setItemMeta(metaElixir);

        ItemStack elixirN = new ItemStack(Material.CHARCOAL);
        ItemMeta metaElixirN = elixirN.getItemMeta();
        metaElixirN.setDisplayName(Main.getTradution("@AuM&UevY6FRX53", uuid));
        ArrayList<String> loreElixirN = new ArrayList<>();
        loreElixirN.add(" ");
        loreElixirN.add(Main.getTradution("4s2F*sm@$Dnv9eZ", uuid));
        metaElixirN.setLore(loreElixirN);
        elixirN.setItemMeta(metaElixirN);

        ItemStack sair = new ItemStack(Material.OAK_SIGN);
        ItemMeta metaSair = sair.getItemMeta();
        metaSair.setDisplayName(Main.getTradution("k9aFdNy%AT5NnvY", uuid));
        ArrayList<String> loreSair = new ArrayList<>();
        loreSair.add(" ");
        loreSair.add(Main.getTradution("SF9JuWHWAsA?UfC", uuid));
        metaSair.setLore(loreSair);
        sair.setItemMeta(metaSair);

        inventory.setItem(11, plot);
        inventory.setItem(13, coins);
        inventory.setItem(14, elixir);
        inventory.setItem(15, elixirN);
        inventory.setItem(22, coinsStock);
        inventory.setItem(23, elixirS);
        inventory.setItem(24, elixirNS);
        inventory.setItem(35, sair);

        return inventory;
    }

    public static Inventory getUpgradePlot(String uuid) {
        Inventory inventory = Bukkit.createInventory(null, 3 * 9, Main.getTradution("hR5tUv@?z45kSXB", uuid));

        ItemStack info = new ItemStack(Material.BOOK);
        ItemMeta meta = info.getItemMeta();
        meta.setDisplayName(Main.getTradution("VWnc6E4Y$rKTqV3", uuid));
        ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(Main.getTradution("rYHbS4R!UkWph#z", uuid) + ChatColor.GREEN + GuildaDb.getGuildaP(uuid));
        lore.add(Main.getTradution("V%UX#J9Gby#B8h&", uuid) + ChatColor.GREEN + GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)));
        lore.add(" ");
        if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) < 8) {
            //Limites = Coins 5.000.000 > Elixir 2.500.000 > elixir_negro 500.000
            //
            //2 = 500.000 coins  -  12
            //3 = 1.750.000 coins  -  24
            //4 = 3.250.000 coins  -  36
            //5 = 750.000 elixir  -  48
            //6 = 1.500.000 elixir  -  60
            //7 = 2.250.000 elixir  -  72
            //8 = 400.000 elixir negro  -  84
            if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1 == 2) {
                lore.add(Main.getTradution("&%%ss4d3@8&s7Ak", uuid) + ChatColor.YELLOW + "500.000 coins");
            } else if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1 == 3) {
                lore.add(Main.getTradution("&%%ss4d3@8&s7Ak", uuid) + ChatColor.YELLOW + "1.750.000 coins");
            } else if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1 == 4) {
                lore.add(Main.getTradution("&%%ss4d3@8&s7Ak", uuid) + ChatColor.YELLOW + "3.250.000 coins");
            } else if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1 == 5) {
                lore.add(Main.getTradution("&%%ss4d3@8&s7Ak", uuid) + ChatColor.RED + "750.000 elixir");
            } else if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1 == 6) {
                lore.add(Main.getTradution("&%%ss4d3@8&s7Ak", uuid) + ChatColor.RED + "1.500.000 elixir");
            } else if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1 == 7) {
                lore.add(Main.getTradution("&%%ss4d3@8&s7Ak", uuid) + ChatColor.RED + "2.250.000 elixir");
            } else if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1 == 8) {
                lore.add(Main.getTradution("&%%ss4d3@8&s7Ak", uuid) + ChatColor.DARK_PURPLE + "400.000 elixir negro");
            }
            lore.add(Main.getTradution("9c!Eu5&ZjKpkVQQ", uuid) + ChatColor.GREEN + "Lv" + (12 * GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid))));
            lore.add(Main.getTradution("Ga*%$FZs5#4R*XS", uuid) + ChatColor.GREEN + (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) + 1));
            lore.add(" ");
        }
        meta.setLore(lore);
        info.setItemMeta(meta);

        ItemStack cancel = new ItemStack(Material.RED_WOOL);
        ItemMeta metaCancel = cancel.getItemMeta();
        metaCancel.setDisplayName(Main.getTradution("C9%3CcWj9?5TQZU", uuid));
        ArrayList<String> loreCancel = new ArrayList<>();
        loreCancel.add(" ");
        loreCancel.add(Main.getTradution("%MeU$tW594r8qJz", uuid));
        metaCancel.setLore(loreCancel);
        cancel.setItemMeta(metaCancel);

        ItemStack confirm = new ItemStack(Material.LIME_WOOL);
        ItemMeta metaConf = confirm.getItemMeta();
        metaConf.setDisplayName(Main.getTradution("&u$Nc&Y3qUW*8HD", uuid));
        ArrayList<String> loreConf = new ArrayList<>();
        loreConf.add(" ");
        loreConf.add(Main.getTradution("qw!wK7BDYBSK&7b", uuid));
        metaConf.setLore(loreConf);
        confirm.setItemMeta(metaConf);

        inventory.setItem(11, info);
        inventory.setItem(14, cancel);
        inventory.setItem(15, confirm);
        return inventory;
    }
}
