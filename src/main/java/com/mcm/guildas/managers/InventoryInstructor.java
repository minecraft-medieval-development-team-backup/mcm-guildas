package com.mcm.guildas.managers;

import com.mcm.core.Main;
import com.mcm.core.database.GuildaDb;
import com.mcm.core.utils.RemoveAllFlags;
import com.mcm.core.utils.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class InventoryInstructor {

    public static DecimalFormat formatter = new DecimalFormat("#,###.00");

    public static Inventory get(String uuid) {
        Inventory inventory = Bukkit.createInventory(null, 3 * 9, Main.getTradution("eVv%8a35&&P39x@", uuid));

        ItemStack offline = new ItemStack(Material.EMERALD);
        ItemMeta metaOffline = offline.getItemMeta();
        metaOffline.setDisplayName(Main.getTradution("4rz%RaEnG2Z9T*%", uuid));
        offline.setItemMeta(metaOffline);

        ItemStack online = new ItemStack(Material.EXPERIENCE_BOTTLE);
        ItemMeta metaOnline = online.getItemMeta();
        metaOnline.setDisplayName(Main.getTradution("6e6rf2tVxMcH@!z", uuid));
        online.setItemMeta(metaOnline);

        //Guilda name > Guilda level > Guilda xp > Guilda membros > Plot level > Coins > Elixir > Elixir negro > / > Wins > Defeats > Último ataque sofrido > Último ataque efetuado
        ItemStack infos = new ItemStack(Material.BOOK);
        ItemMeta metaInfo = infos.getItemMeta();
        metaInfo.setDisplayName(Main.getTradution("TrXFAjCDNdA9#GS", uuid));
        ArrayList<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add(Main.getTradution("Q!s6658xgFt!Vu@", uuid) + ChatColor.GREEN + GuildaDb.getGuildaP(uuid));
        lore.add(Main.getTradution("M82j!&qEEhPvDb&", uuid) + ChatColor.GREEN + GuildaDb.getLevel(GuildaDb.getGuildaP(uuid)));
        lore.add(Main.getTradution("Ra3&K8a2eEmJAPX", uuid) + ChatColor.GREEN + GuildaDb.getXp(GuildaDb.getGuildaP(uuid)));
        String membros = null;
        if (GuildaDb.getMembros(GuildaDb.getGuildaP(uuid)).contains(":")) {
            for (String user : GuildaDb.getMembros(GuildaDb.getGuildaP(uuid)).split(":")) {
                String[] split = user.split("#");
                if (membros == null) membros = split[0];
                else membros += ", " + split[0];
            }
        } else membros = GuildaDb.getMembros(GuildaDb.getGuildaP(uuid));
        lore.add(Main.getTradution("XE!x7Zv9QPj@d*8", uuid) + ChatColor.GREEN + membros);
        lore.add(Main.getTradution("V%UX#J9Gby#B8h&", uuid) + ChatColor.GREEN + GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)));
        lore.add(Main.getTradution("54VZ7#Tzw2ZmsgR", uuid) + ChatColor.GREEN + formatter.format(GuildaDb.getCoins(GuildaDb.getGuildaP(uuid))));
        lore.add(Main.getTradution("F?@RcD?xx#6pH3k", uuid) + ChatColor.GREEN + formatter.format(GuildaDb.getElixir(GuildaDb.getGuildaP(uuid))));
        lore.add(Main.getTradution("6cA333dg$p&bmZT", uuid) + ChatColor.GREEN + formatter.format(GuildaDb.getelixir_negro(GuildaDb.getGuildaP(uuid))));
        lore.add(" ");
        lore.add(Main.getTradution("*2eMJffS89sK*Ke", uuid) + ChatColor.GREEN + GuildaDb.getWins(GuildaDb.getGuildaP(uuid)));
        lore.add(Main.getTradution("QxRH8zSm@bpFB&g", uuid) + ChatColor.GREEN + GuildaDb.getDefeats(GuildaDb.getGuildaP(uuid)));
        if (GuildaDb.getLastAttackSuffered(GuildaDb.getGuildaP(uuid)) != null) lore.add(Main.getTradution("qj?PnFMfN3JT3w#", uuid) + ChatColor.GREEN + TimeUtil.getTime(GuildaDb.getLastAttackSuffered(GuildaDb.getGuildaP(uuid)).getTime()));
        else lore.add(Main.getTradution("qj?PnFMfN3JT3w#", uuid));
        if (GuildaDb.getLastAttackCarried(GuildaDb.getGuildaP(uuid)) != null) lore.add(Main.getTradution("Fxdqe%8qsWHP2Ce", uuid) + ChatColor.GREEN + TimeUtil.getTime(GuildaDb.getLastAttackCarried(GuildaDb.getGuildaP(uuid)).getTime()));
        else lore.add(Main.getTradution("Fxdqe%8qsWHP2Ce", uuid));
        lore.add(" ");
        metaInfo.setLore(lore);
        infos.setItemMeta(metaInfo);

        inventory.setItem(11, infos);
        inventory.setItem(14, RemoveAllFlags.remove(online));
        inventory.setItem(15, RemoveAllFlags.remove(offline));
        return inventory;
    }
}
