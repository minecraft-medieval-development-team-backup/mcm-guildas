package com.mcm.guildas.managers;

import com.mcm.core.Main;
import com.mcm.core.utils.EnchantmentGlow;
import com.mcm.core.utils.RemoveAllFlags;
import lombok.var;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class InventoryShop {

    public static Inventory get(String uuid) {
        Inventory inventory = Bukkit.createInventory(null, 3 * 9, Main.getTradution("b4y9E$nbN!bKCG&", uuid));

        ItemStack ferramentas = new ItemStack(Material.DIAMOND_PICKAXE);
        ItemMeta metaFer = ferramentas.getItemMeta();
        metaFer.setDisplayName(Main.getTradution("JQEmkcB6QGnWr&a", uuid));
        ferramentas.setItemMeta(metaFer);

        ItemStack armas = new ItemStack(Material.GOLDEN_SWORD);
        ItemMeta metaArmas = armas.getItemMeta();
        metaArmas.setDisplayName(Main.getTradution("T5ryTp%x4T7v96Z", uuid));
        armas.setItemMeta(metaArmas);

        ItemStack explosives = new ItemStack(Material.TNT);
        ItemMeta metaExplo = explosives.getItemMeta();
        metaExplo.setDisplayName(Main.getTradution("Qkugs#Qg3gpma?G", uuid));
        explosives.setItemMeta(metaExplo);

        ItemStack machines = new ItemStack(Material.BLAST_FURNACE);
        ItemMeta metaMachi = machines.getItemMeta();
        metaMachi.setDisplayName(Main.getTradution("#@zShF3f3ZwGcUk", uuid));
        machines.setItemMeta(metaMachi);

        ItemStack protections = new ItemStack(Material.OBSIDIAN);
        ItemMeta metaProtec = protections.getItemMeta();
        metaProtec.setDisplayName(Main.getTradution("9dYtv4v&YvE@tfy", uuid));
        protections.setItemMeta(metaProtec);

        ItemStack gadget = new ItemStack(Material.PAINTING);
        ItemMeta metaGad = gadget.getItemMeta();
        metaGad.setDisplayName(Main.getTradution("Tus$hrD6u#HrS%g", uuid));
        gadget.setItemMeta(metaGad);

        inventory.setItem(10, RemoveAllFlags.remove(ferramentas));
        inventory.setItem(11, RemoveAllFlags.remove(armas));
        inventory.setItem(12, RemoveAllFlags.remove(explosives));
        inventory.setItem(13, RemoveAllFlags.remove(protections));
        inventory.setItem(16, RemoveAllFlags.remove(gadget));

        return inventory;
    }

    //250.000  explosivos> 3 > 5 > 7 / proteções> 4 > 3 > 12 > 16
    public static Inventory getInvProtections(String uuid) {
        Inventory inventory = Bukkit.createInventory(null, 5 * 9, Main.getTradution("ewsGfV&Pnus58np", uuid));

        ItemStack evoker = new ItemStack(Material.EVOKER_SPAWN_EGG);
        ItemMeta metaEvoker = evoker.getItemMeta();
        metaEvoker.setDisplayName(Main.getTradution("#R@%mP$f7c2kkAw", uuid));
        ArrayList<String> loreEvoker = new ArrayList<>();
        loreEvoker.add(Main.getTradution("kFFpmYzb6aw4Cj!", uuid) + ChatColor.GREEN + "15");
        loreEvoker.add(Main.getTradution("Gw&ujSKj3SYw9fD", uuid) + ChatColor.GREEN + "40 " + Main.getTradution("TZmqa5a3meN#qH#", uuid));
        loreEvoker.add(" ");
        loreEvoker.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.DARK_PURPLE + "75.000,00 elixir negro");
        loreEvoker.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaEvoker.setLore(loreEvoker);
        evoker.setItemMeta(metaEvoker);

        ItemStack vindicator = new ItemStack(Material.VINDICATOR_SPAWN_EGG);
        ItemMeta metaVindicator = vindicator.getItemMeta();
        metaVindicator.setDisplayName(Main.getTradution("5P!eXVDwFF5%*$H", uuid));
        ArrayList<String> loreVindicator = new ArrayList<>();
        loreVindicator.add(Main.getTradution("kFFpmYzb6aw4Cj!", uuid) + ChatColor.GREEN + "40");
        loreVindicator.add(Main.getTradution("Gw&ujSKj3SYw9fD", uuid) + ChatColor.GREEN + "25 " + Main.getTradution("TZmqa5a3meN#qH#", uuid));
        loreVindicator.add(" ");
        loreVindicator.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.DARK_PURPLE + "45.000,00 elixir negro");
        loreVindicator.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaVindicator.setLore(loreVindicator);
        vindicator.setItemMeta(metaVindicator);

        ItemStack spiderJocker = new ItemStack(Material.SPIDER_SPAWN_EGG);
        ItemMeta metaSpiderJ = spiderJocker.getItemMeta();
        metaSpiderJ.setDisplayName(Main.getTradution("tuZme53V%Hd3Afj", uuid));
        ArrayList<String> loreSpiderJ = new ArrayList<>();
        loreSpiderJ.add(Main.getTradution("kFFpmYzb6aw4Cj!", uuid) + ChatColor.GREEN + "30");
        loreSpiderJ.add(Main.getTradution("Gw&ujSKj3SYw9fD", uuid) + ChatColor.GREEN + "30 " + Main.getTradution("TZmqa5a3meN#qH#", uuid));
        loreSpiderJ.add(" ");
        loreSpiderJ.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.RED + "115.000,00 elixir");
        loreSpiderJ.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaSpiderJ.setLore(loreSpiderJ);
        spiderJocker.setItemMeta(metaSpiderJ);

        ItemStack skeletonS = new ItemStack(Material.SKELETON_SPAWN_EGG);
        ItemMeta metaSkeletonS = skeletonS.getItemMeta();
        metaSkeletonS.setDisplayName(Main.getTradution("pvyMfw5k%PDj87n", uuid));
        ArrayList<String> loreSkeletonS = new ArrayList<>();
        loreSkeletonS.add(Main.getTradution("kFFpmYzb6aw4Cj!", uuid) + ChatColor.GREEN + "50");
        loreSkeletonS.add(Main.getTradution("Gw&ujSKj3SYw9fD", uuid) + ChatColor.GREEN + "20 " + Main.getTradution("TZmqa5a3meN#qH#", uuid));
        loreSkeletonS.add(" ");
        loreSkeletonS.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.RED + "65.000,00 elixir");
        loreSkeletonS.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaSkeletonS.setLore(loreSkeletonS);
        skeletonS.setItemMeta(metaSkeletonS);

        ItemStack zombieS = new ItemStack(Material.ZOMBIE_SPAWN_EGG);
        ItemMeta metaZombieS = zombieS.getItemMeta();
        metaZombieS.setDisplayName(Main.getTradution("av6kuE6&*#nm7kU", uuid));
        ArrayList<String> loreZombieS = new ArrayList<>();
        loreZombieS.add(Main.getTradution("kFFpmYzb6aw4Cj!", uuid) + ChatColor.GREEN + "50");
        loreZombieS.add(Main.getTradution("Gw&ujSKj3SYw9fD", uuid) + ChatColor.GREEN + "15 " + Main.getTradution("TZmqa5a3meN#qH#", uuid));
        loreZombieS.add(" ");
        loreZombieS.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.RED + "50.000,00 elixir");
        loreZombieS.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaZombieS.setLore(loreZombieS);
        zombieS.setItemMeta(metaZombieS);

        ItemStack door = new ItemStack(Material.DARK_OAK_DOOR);
        ItemMeta metaDoor = door.getItemMeta();
        metaDoor.setDisplayName(Main.getTradution("F&NCGG$Hc7%qP7!", uuid));
        ArrayList<String> loreDoor = new ArrayList<>();
        loreDoor.add(Main.getTradution("QWtDZW2z*uJ#ycz", uuid) + ChatColor.GREEN + "16/16");
        loreDoor.add(Main.getTradution("T7QFd2ReT#ws3x7", uuid));
        loreDoor.add(" ");
        loreDoor.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.RED + "10.000,00 elixir");
        loreDoor.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaDoor.setLore(loreDoor);
        door.setItemMeta(metaDoor);

        ItemStack block4 = new ItemStack(Material.BEDROCK);
        ItemMeta metaBlock4 = block4.getItemMeta();
        metaBlock4.setDisplayName(Main.getTradution("QnBF*zhE8zJDTuG", uuid) + "4");
        ArrayList<String> loreBlock4 = new ArrayList<>();
        loreBlock4.add(Main.getTradution("QWtDZW2z*uJ#ycz", uuid) + ChatColor.GREEN + "16/16");
        loreBlock4.add(Main.getTradution("EkTd78qH4?P#nzR", uuid));
        loreBlock4.add(" ");
        loreBlock4.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.DARK_PURPLE + "1.000,00 elixir negro");
        loreBlock4.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaBlock4.setLore(loreBlock4);
        block4.setItemMeta(metaBlock4);

        ItemStack block3 = new ItemStack(Material.OBSIDIAN);
        ItemMeta metaBlock3 = block3.getItemMeta();
        metaBlock3.setDisplayName(Main.getTradution("QnBF*zhE8zJDTuG", uuid) + "3");
        ArrayList<String> loreBlock3 = new ArrayList<>();
        loreBlock3.add(Main.getTradution("QWtDZW2z*uJ#ycz", uuid) + ChatColor.GOLD + "12" + ChatColor.GREEN + "/16");
        loreBlock3.add(Main.getTradution("EkTd78qH4?P#nzR", uuid));
        loreBlock3.add(" ");
        loreBlock3.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.RED + "1.000,00 elixir");
        loreBlock3.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaBlock3.setLore(loreBlock3);
        block3.setItemMeta(metaBlock3);

        ItemStack block2 = new ItemStack(Material.GRAVEL);
        ItemMeta metaBlock2 = block2.getItemMeta();
        metaBlock2.setDisplayName(Main.getTradution("QnBF*zhE8zJDTuG", uuid) + "2");
        ArrayList<String> loreBlock2 = new ArrayList<>();
        loreBlock2.add(Main.getTradution("QWtDZW2z*uJ#ycz", uuid) + ChatColor.RED + "3" + ChatColor.GREEN + "/16");
        loreBlock2.add(Main.getTradution("K*Fkm2mNCADBwrc", uuid));
        loreBlock2.add(" ");
        loreBlock2.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.GOLD + "175,00 coins");
        loreBlock2.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaBlock2.setLore(loreBlock2);
        block2.setItemMeta(metaBlock2);

        ItemStack block1 = new ItemStack(Material.EMERALD_BLOCK);
        ItemMeta metaBlock1 = block1.getItemMeta();
        metaBlock1.setDisplayName(Main.getTradution("QnBF*zhE8zJDTuG", uuid) + "1");
        ArrayList<String> loreBlock1 = new ArrayList<>();
        loreBlock1.add(Main.getTradution("QWtDZW2z*uJ#ycz", uuid) + ChatColor.RED + "4" + ChatColor.GREEN + "/16");
        loreBlock1.add(Main.getTradution("EkTd78qH4?P#nzR", uuid));
        loreBlock1.add(" ");
        loreBlock1.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.GOLD + "100,00 coins");
        loreBlock1.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaBlock1.setLore(loreBlock1);
        block1.setItemMeta(metaBlock1);

        ItemStack devastator = new ItemStack(Material.IRON_HORSE_ARMOR);
        ItemMeta metaDevast = devastator.getItemMeta();
        metaDevast.setDisplayName(Main.getTradution("7UBJK7*Mtz#e4$P", uuid));
        ArrayList<String> loreDevast = new ArrayList<>();
        loreDevast.add(Main.getTradution("U8#4EFQM?QgCD3&", uuid));
        loreDevast.add(Main.getTradution("$Y9&H9%49dcSX8G", uuid));
        loreDevast.add(Main.getTradution("kTATnWAcbheG9s$", uuid));
        loreDevast.add(" ");
        loreDevast.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.DARK_PURPLE + "200.000,00 elixir negro");
        loreDevast.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaDevast.setLore(loreDevast);
        devastator.setItemMeta(metaDevast);

        ItemStack ironGolem = new ItemStack(Material.IRON_CHESTPLATE);
        ItemMeta metaIronG = ironGolem.getItemMeta();
        metaIronG.setDisplayName(Main.getTradution("m7dFsR!4a3y9bBT", uuid));
        ArrayList<String> loreIronG = new ArrayList<>();
        loreIronG.add(Main.getTradution("gs9Uh#xHX8T2%Hw", uuid));
        loreIronG.add(Main.getTradution("FDHDtY7C@@5yMP@", uuid));
        loreIronG.add(Main.getTradution("67*RsmADKnJzWRr", uuid));
        loreIronG.add(" ");
        loreIronG.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.RED + "750.000 elixir");
        loreIronG.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaIronG.setLore(loreIronG);
        ironGolem.setItemMeta(metaIronG);

        ItemStack vex = new ItemStack(Material.TOTEM_OF_UNDYING);
        ItemMeta metaVex = vex.getItemMeta();
        metaVex.setDisplayName(Main.getTradution("E9eyQ#H&wQEb&2K", uuid));
        ArrayList<String> loreVex = new ArrayList<>();
        loreVex.add(Main.getTradution("9ShE$5@G7%tZaEr", uuid));
        loreVex.add(Main.getTradution("@!S&7DE#7JnrZNa", uuid));
        loreVex.add(Main.getTradution("Syv3rnwH5w&jxg%", uuid));
        loreVex.add(" ");
        loreVex.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.RED + "350.000 elixir");
        loreVex.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaVex.setLore(loreVex);
        vex.setItemMeta(metaVex);

        ItemStack burners = new ItemStack(Material.BLAZE_POWDER);
        ItemMeta metaBurners = burners.getItemMeta();
        metaBurners.setDisplayName(Main.getTradution("E9eyQ#H&wQEb&2K", uuid));
        ArrayList<String> loreBurners = new ArrayList<>();
        loreBurners.add(Main.getTradution("9ShE$5@G7%tZaEr", uuid));
        loreBurners.add(Main.getTradution("@!S&7DE#7JnrZNa", uuid));
        loreBurners.add(Main.getTradution("Syv3rnwH5w&jxg%", uuid));
        loreBurners.add(" ");
        loreBurners.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.RED + "350.000 elixir");
        loreBurners.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaBurners.setLore(loreBurners);
        burners.setItemMeta(metaBurners);

        ItemStack mages = new ItemStack(Material.WITHER_ROSE);
        ItemMeta metaMages = mages.getItemMeta();
        metaMages.setDisplayName(Main.getTradution("Q8SkkuajBx@&*?7", uuid));
        ArrayList<String> loreMages = new ArrayList<>();
        loreMages.add(Main.getTradution("yJWb?9spRNJ4RTM", uuid));
        loreMages.add(Main.getTradution("Qx@g@bPmvyXR2Sr", uuid));
        loreMages.add(Main.getTradution("GJsuRXAba@ESz66", uuid));
        loreMages.add(" ");
        loreMages.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.GOLD + "450.000 coins");
        loreMages.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaMages.setLore(loreMages);
        mages.setItemMeta(metaMages);

        ItemStack guardians = new ItemStack(Material.HEART_OF_THE_SEA);
        ItemMeta metaGuard = guardians.getItemMeta();
        metaGuard.setDisplayName(Main.getTradution("je&c$F?4&qQuyQP", uuid));
        ArrayList<String> loreGuard = new ArrayList<>();
        loreGuard.add(Main.getTradution("v$Cczp765dQujs*", uuid));
        loreGuard.add(Main.getTradution("bbRq%V*E6Ppczcd", uuid));
        loreGuard.add(Main.getTradution("PJNg6Ak&G8mr$Ud", uuid));
        loreGuard.add(" ");
        loreGuard.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.GOLD + "250.000 coins");
        loreGuard.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaGuard.setLore(loreGuard);
        guardians.setItemMeta(metaGuard);

        ItemStack tower = new ItemStack(Material.CHISELED_STONE_BRICKS);
        ItemMeta metaTower = tower.getItemMeta();
        metaTower.setDisplayName(Main.getTradution("N@Qpad8V223w@#c", uuid));
        ArrayList<String> loreTower = new ArrayList<>();
        loreTower.add(Main.getTradution("!nswKKpJp?9wX59", uuid));
        loreTower.add(Main.getTradution("?r*@8V#!J2d$DHj", uuid));
        loreTower.add(" ");
        loreTower.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.GOLD + "150.000 coins");
        loreTower.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaTower.setLore(loreTower);
        tower.setItemMeta(metaTower);

        inventory.setItem(10, RemoveAllFlags.remove(tower));
        inventory.setItem(11, RemoveAllFlags.remove(guardians));
        inventory.setItem(12, RemoveAllFlags.remove(mages));
        inventory.setItem(13, RemoveAllFlags.remove(burners));
        inventory.setItem(14, RemoveAllFlags.remove(ironGolem));
        inventory.setItem(15, RemoveAllFlags.remove(vex));
        inventory.setItem(16, RemoveAllFlags.remove(devastator));
        inventory.setItem(20, RemoveAllFlags.remove(block1));
        inventory.setItem(21, RemoveAllFlags.remove(block2));
        inventory.setItem(22, RemoveAllFlags.remove(block3));
        inventory.setItem(23, RemoveAllFlags.remove(block4));
        inventory.setItem(24, RemoveAllFlags.remove(door));
        inventory.setItem(29, RemoveAllFlags.remove(zombieS));
        inventory.setItem(30, RemoveAllFlags.remove(skeletonS));
        inventory.setItem(31, RemoveAllFlags.remove(spiderJocker));
        inventory.setItem(32, EnchantmentGlow.addGlow(RemoveAllFlags.remove(vindicator)));
        inventory.setItem(33, EnchantmentGlow.addGlow(RemoveAllFlags.remove(evoker)));
        inventory.setItem(36, back(uuid));
        inventory.setItem(44, close(uuid));
        return inventory;
    }

    public static Inventory getInvExplosives(String uuid) {
        Inventory inventory = Bukkit.createInventory(null, 3 * 9, Main.getTradution("#yT*gbWfXP3FNX$", uuid));

        ItemStack fireball = new ItemStack(Material.FIRE_CHARGE);
        ItemMeta metaFireB = fireball.getItemMeta();
        metaFireB.setDisplayName(Main.getTradution("Y3Ht!Tb%6ZgHsdp", uuid));
        ArrayList<String> loreFireB = new ArrayList<>();
        loreFireB.add(Main.getTradution("B@Ej%#*K*YNYC7t", uuid));
        loreFireB.add(Main.getTradution("jx?C&&N9reD8cA2", uuid));
        loreFireB.add(Main.getTradution("r2@CQyu6Av%8Ugu", uuid));
        loreFireB.add(" ");
        loreFireB.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.GOLD + "250.000 coins");
        loreFireB.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaFireB.setLore(loreFireB);
        fireball.setItemMeta(metaFireB);

        ItemStack creeper = new ItemStack(Material.CREEPER_SPAWN_EGG);
        ItemMeta metaCreeper = creeper.getItemMeta();
        metaCreeper.setDisplayName(Main.getTradution("b!vRr4U@dBHvn52", uuid));
        ArrayList<String> loreCreeper = new ArrayList<>();
        loreCreeper.add(Main.getTradution("8gY6#Hbz&p#X9Y&", uuid));
        loreCreeper.add(Main.getTradution("wnZ$F74t3gJ3uG#", uuid));
        loreCreeper.add(Main.getTradution("?Xm4#Zd&Bz8vKK#", uuid));
        loreCreeper.add(" ");
        loreCreeper.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.GOLD + "40.000 coins");
        loreCreeper.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaCreeper.setLore(loreCreeper);
        creeper.setItemMeta(metaCreeper);

        ItemStack tnt = new ItemStack(Material.TNT);
        ItemMeta metaTnt = tnt.getItemMeta();
        metaTnt.setDisplayName(Main.getTradution("Z5mF%D2bNbmQ2b!", uuid));
        ArrayList<String> loreTnt = new ArrayList<>();
        loreTnt.add(Main.getTradution("Uaszb$$%4t2WUuy", uuid));
        loreTnt.add(Main.getTradution("HeAp$dRY8y9#@YJ", uuid));
        loreTnt.add(Main.getTradution("!Qtwz4RK$BZw&b%", uuid));
        loreTnt.add(" ");
        loreTnt.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.GOLD + "10.000 coins");
        loreTnt.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaTnt.setLore(loreTnt);
        tnt.setItemMeta(metaTnt);

        inventory.setItem(12, RemoveAllFlags.remove(tnt));
        inventory.setItem(13, RemoveAllFlags.remove(creeper));
        inventory.setItem(14, RemoveAllFlags.remove(fireball));
        inventory.setItem(18, back(uuid));
        inventory.setItem(26, close(uuid));
        return inventory;
    }

    public static Inventory getInvTools(String uuid) {
        Inventory inventory = Bukkit.createInventory(null, 3 * 9, Main.getTradution("bUg@qb5U&qt6aPA", uuid));

        ItemStack axe = new ItemStack(Material.DIAMOND_AXE);
        ItemMeta axeMeta = axe.getItemMeta();
        axeMeta.setDisplayName(Main.getTradution("UwdZYME@qZQE5d&", uuid));
        ArrayList<String> loreAxe = new ArrayList<>();
        loreAxe.add(Main.getTradution("6dgNnR9?z5&qVRN", uuid));
        loreAxe.add(Main.getTradution("w!%h6QY#yCn7dyA", uuid));
        loreAxe.add(Main.getTradution("5N*Bf6AP8STbfBt", uuid));
        loreAxe.add(" ");
        loreAxe.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.RED + "1.000.000 elixir");
        loreAxe.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        axeMeta.setLore(loreAxe);
        axe.setItemMeta(axeMeta);

        ItemStack shovel = new ItemStack(Material.DIAMOND_SHOVEL);
        ItemMeta metaShovel = shovel.getItemMeta();
        metaShovel.setDisplayName(Main.getTradution("?Fr!A7Rx!XKz6hX", uuid));
        ArrayList<String> loreShovel = new ArrayList<>();
        loreShovel.add(Main.getTradution("2w$9S$Q*zp&QwUU", uuid));
        loreShovel.add(Main.getTradution("#gG%5D$3KyJHrWf", uuid));
        loreShovel.add(" ");
        loreShovel.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.RED + "1.750.000 elixir");
        loreShovel.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaShovel.setLore(loreShovel);
        shovel.setItemMeta(metaShovel);

        ItemStack pickaxe0 = new ItemStack(Material.IRON_PICKAXE);
        ItemMeta metaPick0 = pickaxe0.getItemMeta();
        metaPick0.setDisplayName(Main.getTradution("a%cE7rWB22vr#tP", uuid));
        ArrayList<String> lorePick0 = new ArrayList<>();
        lorePick0.add(Main.getTradution("Rb6y5@vAb&y!cpr", uuid));
        lorePick0.add(Main.getTradution("WEK##x6n#NQrN8H", uuid));
        lorePick0.add(" ");
        lorePick0.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.GOLD + "100.000 coins");
        lorePick0.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaPick0.setLore(lorePick0);
        pickaxe0.setItemMeta(metaPick0);

        ItemStack pickaxe1 = new ItemStack(Material.DIAMOND_PICKAXE);
        ItemMeta metaPick1 = pickaxe1.getItemMeta();
        metaPick1.setDisplayName(Main.getTradution("N3yJJ!D#W@nG6$z", uuid));
        ArrayList<String> lorePick1 = new ArrayList<>();
        lorePick1.add(Main.getTradution("jq5QTjDtKe?H%26", uuid));
        lorePick1.add(Main.getTradution("KTZsVB%?&h7Xasx", uuid));
        lorePick1.add(" ");
        lorePick1.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.GOLD + "500.000 coins");
        lorePick1.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaPick1.setLore(lorePick1);
        pickaxe1.setItemMeta(metaPick1);

        ItemStack pickaxe2 = new ItemStack(Material.GOLDEN_PICKAXE);
        ItemMeta metaPick2 = pickaxe2.getItemMeta();
        metaPick2.setDisplayName(Main.getTradution("De#%B5MZqWe9Se&", uuid));
        ArrayList<String> lorePick2 = new ArrayList<>();
        lorePick2.add(Main.getTradution("f#Sq??Gv3&gmP!7", uuid));
        lorePick2.add(Main.getTradution("3XAaDX!v@TKh#32", uuid));
        lorePick2.add(" ");
        lorePick2.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.GOLD + "500.000 coins");
        lorePick2.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaPick2.setLore(lorePick2);
        pickaxe2.setItemMeta(metaPick2);

        ItemStack pickaxe3 = new ItemStack(Material.DIAMOND_PICKAXE);
        ItemMeta metaPick3 = pickaxe3.getItemMeta();
        metaPick3.setDisplayName(Main.getTradution("N3yJJ!D#W@nG6$z", uuid));
        ArrayList<String> lorePick3 = new ArrayList<>();
        lorePick3.add(Main.getTradution("jq5QTjDtKe?H%26", uuid));
        lorePick3.add(Main.getTradution("KTZsVB%?&h7Xasx", uuid));
        lorePick3.add(" ");
        lorePick3.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.RED + "2.000.000 elixir");
        lorePick3.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaPick3.setLore(lorePick3);
        pickaxe3.setItemMeta(metaPick3);

        ItemStack pickaxe4 = new ItemStack(Material.DIAMOND_PICKAXE);
        ItemMeta metaPick4 = pickaxe4.getItemMeta();
        metaPick4.setDisplayName(Main.getTradution("6RZ7?CDj7gY#UsA", uuid));
        ArrayList<String> lorePick4 = new ArrayList<>();
        lorePick4.add(Main.getTradution("G5hhNmYKE*R2nj3", uuid));
        lorePick4.add(Main.getTradution("pFTd%d&H6syPXuW", uuid));
        lorePick4.add(" ");
        lorePick4.add(Main.getTradution("48PnWGHpqy@shkw", uuid) + ChatColor.DARK_PURPLE + "400.000 elixir negro");
        lorePick4.add(Main.getTradution("7dWUEhU@!3RsFaN", uuid));
        metaPick4.setLore(lorePick4);
        pickaxe4.setItemMeta(metaPick4);

        inventory.setItem(10, RemoveAllFlags.remove(axe));
        inventory.setItem(11, RemoveAllFlags.remove(shovel));
        inventory.setItem(12, RemoveAllFlags.remove(pickaxe0));
        inventory.setItem(13, RemoveAllFlags.remove(pickaxe1));
        inventory.setItem(14, RemoveAllFlags.remove(pickaxe2));
        inventory.setItem(15, RemoveAllFlags.remove(pickaxe3));
        inventory.setItem(16, RemoveAllFlags.remove(pickaxe4));
        inventory.setItem(18, back(uuid));
        inventory.setItem(26, close(uuid));
        return inventory;
    }

    private static ItemStack back(String uuid) {
        ItemStack back = new ItemStack(Material.ARROW);
        ItemMeta metaBack = back.getItemMeta();
        metaBack.setDisplayName(Main.getTradution("NF@p4kK&Pk5Z$V%", uuid));
        ArrayList<String> loreBack = new ArrayList<>();
        loreBack.add(" ");
        loreBack.add(Main.getTradution("Zzh5y3?7EFzR3Ff", uuid));
        metaBack.setLore(loreBack);
        back.setItemMeta(metaBack);
        return RemoveAllFlags.remove(back);
    }

    private static ItemStack close(String uuid) {
        ItemStack close = new ItemStack(Material.OAK_SIGN);
        ItemMeta metaClose = close.getItemMeta();
        metaClose.setDisplayName(Main.getTradution("k9aFdNy%AT5NnvY", uuid));
        ArrayList<String> loreClose = new ArrayList<>();
        loreClose.add(" ");
        loreClose.add(Main.getTradution("SF9JuWHWAsA?UfC", uuid));
        metaClose.setLore(loreClose);
        close.setItemMeta(metaClose);
        return RemoveAllFlags.remove(close);
    }
}
