package com.mcm.guildas.listeners;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.mcm.core.Main;
import com.mcm.core.cache.OnlineCount;
import com.mcm.core.database.GuildaDb;
import com.mcm.core.enums.GuildsServers;
import com.mcm.core.serverswitch.PlayerSwitch;
import com.mcm.guildas.hashs.TryTeleport;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PortalTeleportToBase implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();

        if (isOnPortal(player.getLocation())) {
            if (GuildaDb.getGuildaP(player.getUniqueId().toString()) != null) {
                if (GuildaDb.getAlocated(GuildaDb.getGuildaP(player.getUniqueId().toString())) != null) {
                    if (TryTeleport.get(player.getUniqueId().toString()) == null) {
                        new TryTeleport(player.getUniqueId().toString()).insert();
                        Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
                            TryTeleport.get(player.getUniqueId().toString()).delete();
                        }, 100L);

                        connect(player, player.getUniqueId().toString(), GuildaDb.getAlocated(GuildaDb.getGuildaP(player.getUniqueId().toString())));
                    }
                }
                for (GuildsServers server : GuildsServers.values()) {
                    if (OnlineCount.get(server.name().replaceAll("_", "-")) != null && OnlineCount.get(server.name().replaceAll("_", "-")).getCount() < 80) {
                        if (TryTeleport.get(player.getUniqueId().toString()) == null) {
                            new TryTeleport(player.getUniqueId().toString()).insert();
                            Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
                                TryTeleport.get(player.getUniqueId().toString()).delete();
                            }, 100L);

                            connect(player, player.getUniqueId().toString(), server.name().replaceAll("_", "-"));
                        }
                    }
                }
            }
        }
    }

    public static boolean isOnPortal(final Location loc) {
        Location pos1 = new Location(Bukkit.getWorld("world"), 899, 81, -207);
        Location pos2 = new Location(Bukkit.getWorld("world"), 897, 86, -207);

        final int p1x = pos1.getBlockX();
        final int p1y = pos1.getBlockY();
        final int p1z = pos1.getBlockZ();
        final int p2x = pos2.getBlockX();
        final int p2y = pos2.getBlockY();
        final int p2z = pos2.getBlockZ();

        final int minX = p1x < p2x ? p1x : p2x;
        final int minY = p1y < p2y ? p1y : p2y;
        final int minZ = p1z < p2z ? p1z : p2z;

        final int maxX = p1x > p2x ? p1x : p2x;
        final int maxY = p1y > p2y ? p1y : p2y;
        final int maxZ = p1z > p2z ? p1z : p2z;

        if ((loc.getBlockX() >= minX) && (loc.getBlockX() <= maxX) && (loc.getBlockY() >= minY)
                && (loc.getBlockY() <= maxY) && (loc.getBlockZ() >= minZ) && (loc.getBlockZ() <= maxZ)) {
            return true;
        }
        return false;
    }

    public static void connect(Player player, String uuid, String server_name) {
        if (!server_name.equals(Main.server_name)) {
            try {
                boolean sended = PlayerSwitch.sendData(player, server_name);

                if (sended == true) {
                    ByteArrayDataOutput out = ByteStreams.newDataOutput();
                    out.writeUTF("Connect");
                    out.writeUTF(server_name);

                    player.sendPluginMessage(Main.plugin, "BungeeCord", out.toByteArray());
                    player.sendMessage(Main.getTradution("wDcuNh4VjRY#k5c", uuid) + server_name);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);
                }
            } catch (Exception e) {
                if (OnlineCount.get(server_name) == null) {
                    player.sendMessage(Main.getTradution("yKx%z3mb5xnVVGs", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                } else {
                    player.sendMessage(Main.getTradution("4$DSeDuDTnzAhy&", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        } else {
            player.sendMessage(Main.getTradution("D$nS83k4#*bYAPc", uuid));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
    }
}
