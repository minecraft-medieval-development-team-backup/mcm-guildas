package com.mcm.guildas.listeners;

import com.mcm.core.Main;
import com.mcm.core.database.GuildaDb;
import com.mcm.core.enums.GuildsServers;
import com.mcm.guildas.utils.PlotUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerInteractPlot implements Listener {

    /**
     * If the player is trying left of the plot limits
     * @param event
     */
    @EventHandler
    public void onLimits(PlayerMoveEvent event) {
        for (GuildsServers server : GuildsServers.values()) if (server.name().replaceAll("_", "-").equals(Main.server_name)) {
            if (PlotUtil.isOnLimits(event.getTo(), GuildaDb.getGuildaP(event.getPlayer().getUniqueId().toString())) == false) {
                event.setCancelled(true);
                event.getPlayer().teleport(event.getFrom());
            }
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        for (GuildsServers server : GuildsServers.values()) if (server.name().replaceAll("_", "-").equals(Main.server_name)) {
            if (!PlotUtil.isOnArea(event.getBlock().getLocation(), GuildaDb.getGuildaP(uuid))) {
                event.setCancelled(true);

                if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) >= 8) {
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                } else {
                    player.sendMessage(Main.getTradution("2?BW4?bUVek8aG5", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        for (GuildsServers server : GuildsServers.values()) if (server.name().replaceAll("_", "-").equals(Main.server_name)) {
            if (!PlotUtil.isOnArea(event.getBlock().getLocation(), GuildaDb.getGuildaP(uuid))) {
                event.setCancelled(true);

                if (GuildaDb.getPlotLevel(GuildaDb.getGuildaP(uuid)) >= 8) {
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                } else {
                    player.sendMessage(Main.getTradution("2?BW4?bUVek8aG5", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            }
        }
    }

    @EventHandler
    public void entityExplode(EntityExplodeEvent event) {
        for (GuildsServers server : GuildsServers.values()) if (server.name().replaceAll("_", "-").equals(Main.server_name)) {
            ArmorStand infos = null;
            for (Entity entity : event.getLocation().getNearbyEntities(100, 100, 100)) {
                if (entity instanceof ArmorStand) {
                    if (entity.getCustomName().contains("plot-info")) infos = (ArmorStand) entity;
                }
            }

            if (infos != null) {
                String[] split = infos.getCustomName().split(":");
                for (Block block : event.blockList()) {
                    if (!PlotUtil.isOnArea(block.getLocation(), split[1])) {
                        final BlockState state = block.getState();

                        block.setType(Material.AIR);

                        int delay = 20;
                        if ((block.getType().equals(Material.SAND)) || (block.getType().equals(Material.GRAVEL))) {
                            delay += 1;
                        }

                        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.plugin, () -> {
                            state.update(true, false);
                        }, delay);
                    }
                }
            }
        }
    }

    @EventHandler
    public void liquidMove(BlockFromToEvent event) {
        for (GuildsServers server : GuildsServers.values()) if (server.name().replaceAll("_", "-").equals(Main.server_name)) {
            ArmorStand infos = null;
            for (Entity entity : event.getToBlock().getLocation().getNearbyEntities(100, 100, 100)) {
                if (entity instanceof ArmorStand) {
                    if (entity.getCustomName().contains("plot-info")) infos = (ArmorStand) entity;
                }
            }

            if (infos != null) {
                String[] split = infos.getCustomName().split(":");
                if (!PlotUtil.isOnArea(event.getToBlock().getLocation(), split[1])) {
                    event.setCancelled(true);
                }
            }
        }
    }
}
