package com.mcm.guildas.listeners;

import com.mcm.core.Main;
import com.mcm.core.database.GuildaDb;
import com.mcm.core.utils.NBTTagUtil;
import com.mcm.core.utils.TimeUtil;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Date;

public class MiningCollectEvent implements Listener {

    /**
     * TODO * Set here the minings locations!
     */
    public static String coinsMining = "world, 0, 0, 0";
    public static String elixirMining = "world, 0, 0, 0";
    public static String elixirNegroMining = "world, 0, 0, 0";

    private static int coins24h = 500000;
    private static int elixir24h = 250000;
    private static int elixirNegro24h = 50000;

    //20 niveis > nivel-20 = 500.000,00 coins > 250.000,00 elixir > 50.000,00 elixir negro
    @EventHandler
    public void onClick(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (event.getAction().name().equals("RIGHT_CLICK_BLOCK") && event.getClickedBlock().getType().name().contains("SIGN")) {
            String signLoc = event.getClickedBlock().getWorld().getName() + ", " + event.getClickedBlock().getLocation().getBlockX() + ", " + event.getClickedBlock().getLocation().getBlockY() + ", " + event.getClickedBlock().getLocation().getBlockZ();

            try {
                if (coinsMining.equals(signLoc)) {
                    if (((new Date().getTime() - GuildaDb.getLastCoinsCollected(GuildaDb.getGuildaP(uuid))) / 1000) >= 3600) {
                        long difference = (new Date().getTime() - GuildaDb.getLastCoinsCollected(GuildaDb.getGuildaP(uuid))) / 1000;
                        double perSecond = coins24h / 86400;
                        haveStockSpace(player, "coins");
                        double update = (perSecond * difference) + GuildaDb.getCoins(GuildaDb.getGuildaP(uuid));

                        if (update > getLimit("coins", GuildaDb.getGuildaP(uuid)))
                            update = getLimit("coins", GuildaDb.getGuildaP(uuid));
                        GuildaDb.updateCoins(GuildaDb.getGuildaP(uuid), (int) update);
                        GuildaDb.updateLastCoinsCollected(GuildaDb.getGuildaP(uuid));
                    } else sendMsg(player, GuildaDb.getLastCoinsCollected(GuildaDb.getGuildaP(uuid)));
                } else if (elixirMining.equals(signLoc) && GuildaDb.getElixirStockLevel(GuildaDb.getGuildaP(uuid)) >= 1) {
                    if (((new Date().getTime() - GuildaDb.getLastElixirCollected(GuildaDb.getGuildaP(uuid))) / 1000) >= 3600) {
                        long difference = (new Date().getTime() - GuildaDb.getLastElixirCollected(GuildaDb.getGuildaP(uuid))) / 1000;
                        double perSecond = elixir24h / 86400;
                        haveStockSpace(player, "elixir");
                        double update = (perSecond * difference) + GuildaDb.getElixir(GuildaDb.getGuildaP(uuid));

                        if (update > getLimit("elixir", GuildaDb.getGuildaP(uuid)))
                            update = getLimit("elixir", GuildaDb.getGuildaP(uuid));
                        GuildaDb.updateElixir(GuildaDb.getGuildaP(uuid), (int) update);
                        GuildaDb.updateLastElixirCollected(GuildaDb.getGuildaP(uuid));
                    } else sendMsg(player, GuildaDb.getLastElixirCollected(GuildaDb.getGuildaP(uuid)));
                } else if (elixirNegroMining.equals(signLoc) && GuildaDb.getelixir_negroStockLevel(GuildaDb.getGuildaP(uuid)) >= 1) {
                    if (((new Date().getTime() - GuildaDb.getLastElixirNegroCollected(GuildaDb.getGuildaP(uuid))) / 1000) >= 3600) {
                        long difference = (new Date().getTime() - GuildaDb.getLastElixirNegroCollected(GuildaDb.getGuildaP(uuid))) / 1000;
                        double perSecond = elixirNegro24h / 86400;
                        haveStockSpace(player, "elixir_negro");
                        double update = (perSecond * difference) + GuildaDb.getelixir_negro(GuildaDb.getGuildaP(uuid));

                        if (update > getLimit("elixir_negro", GuildaDb.getGuildaP(uuid)))
                            update = getLimit("elixir_negro", GuildaDb.getGuildaP(uuid));
                        GuildaDb.updateElixirNegro(GuildaDb.getGuildaP(uuid), (int) update);
                        GuildaDb.updateLastElixirNegroCollected(GuildaDb.getGuildaP(uuid));
                    } else sendMsg(player, GuildaDb.getLastElixirNegroCollected(GuildaDb.getGuildaP(uuid)));
                }
                player.sendMessage(Main.getTradution("9*sZ!K%5@JCGhaY", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
    }

    public static void haveStockSpace(Player player, String type) {
        if (GuildaDb.getCoins(GuildaDb.getGuildaP(player.getUniqueId().toString())) >= getLimit(type, GuildaDb.getGuildaP(player.getUniqueId().toString()))) {
            player.sendMessage(Main.getTradution("x*czTrEt4RX?%Hf", player.getUniqueId().toString()));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            return;
        }
    }

    private static int getLimit(String type, String guild) {
        if (type.equals("coins")) {
            return (250000 * GuildaDb.getCoinsStockLevel(guild));
        } else if (type.equals("elixir")) {
            return (125000 * GuildaDb.getElixirStockLevel(guild));
        } else if (type.equals("elixir_negro")) {
            return (25000 * GuildaDb.getelixir_negroStockLevel(guild));
        } else return 0;
    }

    private static void sendMsg(Player player, long lastCollected) {
        player.sendMessage(Main.getTradution("%9zskyY@TTdtg9T", player.getUniqueId().toString()) + TimeUtil.getTime((new Date().getTime() - lastCollected) / 1000));
        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
    }
}
