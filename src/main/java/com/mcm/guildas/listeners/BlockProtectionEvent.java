package com.mcm.guildas.listeners;

import com.mcm.core.Main;
import com.mcm.core.database.GuildaDb;
import com.mcm.core.utils.NBTTagUtil;
import com.mcm.guildas.utils.BlockLifeUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

import java.util.ArrayList;

public class BlockProtectionEvent implements Listener {

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        String guildOwner = NBTTagUtil.getStringData(event.getBlock(), "guild");
        if (guildOwner != null && !GuildaDb.getGuildaP(uuid).equals(guildOwner)) {
            if (NBTTagUtil.getStringData(player.getInventory().getItemInMainHand(), "pickaxe_anti_blocks") == null) {
                event.setCancelled(true);
                player.sendMessage(Main.getTradution("ytEuUWqBTSc8P22g", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                return;
            }

            BlockLifeUtil.negativeLife(event.getBlock());
            if (Integer.valueOf(BlockLifeUtil.getLife(event.getBlock())) >= 1) {
                event.setCancelled(true);
            }
        }
    }

    //3 > 5 > 7
    @EventHandler
    public void entityExplode(EntityExplodeEvent event) {
        int cause = 0;
        if (event.getEntityType().equals(EntityType.PRIMED_TNT)) cause = 3;
        if (event.getEntityType().equals(EntityType.CREEPER)) cause = 5;
        if (event.getEntityType().equals(EntityType.FIREBALL)) cause = 7;

        ArrayList<Block> regenBlocks = new ArrayList<>();

        /**
         * TODO * Check if is necessarily use the list > regenBlocks
         */
        if (cause != 0) {
            for (Block block : event.blockList()) {
                if (BlockLifeUtil.getLife(block) != null) {
                    BlockLifeUtil.negativeLife(block);
                    if (Integer.valueOf(BlockLifeUtil.getLife(block)) >= 1) {
                        regenBlocks.add(block);
                        event.blockList().remove(block);
                    }
                }
            }

            /*
            /**
             * Regening blocks
            Bukkit.getScheduler().runTaskLater(Main.plugin, () -> {
                if (!regenBlocks.isEmpty()) {
                    for (Block block : regenBlocks) {
                        block.getLocation().getBlock().setType(block.getType());
                        Block newBlock = block.getLocation().getBlock();
                        NBTTagUtil.setData(newBlock, "durability", NBTTagUtil.getStringData(block, "durability"));
                        NBTTagUtil.setData(newBlock, "last_update", NBTTagUtil.getStringData(block, "last_update"));
                        NBTTagUtil.setData(newBlock, "type", NBTTagUtil.getStringData(block, "type"));
                    }
                }
            }, 5L);
            */
        }
    }
}
