package com.mcm.guildas.listeners;

import com.mcm.core.Main;
import com.mcm.core.database.GuildaDb;
import com.mcm.core.enums.GuildsServers;
import com.mcm.guildas.hashs.Plots;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitEvents implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        for (GuildsServers server : GuildsServers.values()) if (server.name().replaceAll("_", "-").equals(Main.server_name)) {
            String guild = GuildaDb.getGuildaP(uuid);
            boolean have = false;
            for (Player target : Bukkit.getOnlinePlayers()) {
                if (!target.getName().equals(player.getName()) && GuildaDb.getGuildaP(target.getUniqueId().toString()).equals(guild))
                    have = true;
            }

            if (!have) {
                Plots.get(guild).delete();
            }
        }
    }
}
