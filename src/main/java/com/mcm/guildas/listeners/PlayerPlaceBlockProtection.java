package com.mcm.guildas.listeners;

import com.mcm.core.Main;
import com.mcm.core.database.GuildaDb;
import com.mcm.core.utils.NBTTagUtil;
import com.mcm.guildas.utils.BlockLifeUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Date;

public class PlayerPlaceBlockProtection implements Listener {

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        String guildOwner = NBTTagUtil.getStringData(event.getBlock(), "guild");
        if (guildOwner != null && GuildaDb.getGuildaP(uuid).equals(guildOwner)) {
            event.setCancelled(true);

            /**
             * TODO * Check if this drop correctly and make sound of block break!
             */
            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), NBTTagUtil.setData(new ItemStack(event.getBlock().getType()), "type", NBTTagUtil.getStringData(event.getBlock(), "type")));
            event.getBlock().setType(Material.AIR);
        }
    }

    @EventHandler
    public void onPlace(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (event.getAction().name().equals("RIGHT_CLICK_BLOCK")) {
            if (player.getItemInHand() != null && !player.getItemInHand().getType().equals(Material.AIR)) {
                if (player.getItemInHand().getType().equals(Material.STICK)) {
                    if (BlockLifeUtil.getLife(event.getClickedBlock()) != null) player.sendMessage(Main.getTradution("TtHTxXc6hw2P?37", uuid) + BlockLifeUtil.getLife(event.getClickedBlock()));
                }

                if (NBTTagUtil.getStringData(player.getInventory().getItemInMainHand(), "type") != null) {
                    event.setCancelled(true);

                    Bukkit.broadcastMessage("passou");
                    /**
                     * TODO * Confirm if this block is correctly!
                     */
                    Block place = event.getClickedBlock().getRelative(event.getBlockFace());
                    place.setType(player.getItemInHand().getType());
                    player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
                    player.playSound(place.getLocation(), place.getSoundGroup().getPlaceSound(), 1.0f, 1.0f);
                    if (NBTTagUtil.getStringData(player.getItemInHand(), "type").equals("protection-1")) NBTTagUtil.setData(place, "durability", "4"); else if (NBTTagUtil.getStringData(player.getItemInHand(), "type").equals("protection-2")) NBTTagUtil.setData(place, "durability", "3");
                    else if (NBTTagUtil.getStringData(player.getItemInHand(), "type").equals("protection-3")) NBTTagUtil.setData(place, "durability", "12"); else if (NBTTagUtil.getStringData(player.getItemInHand(), "type").equals("protection-4")) NBTTagUtil.setData(place, "durability", "16");
                    NBTTagUtil.setData(place, "last_update", String.valueOf(new Date().getTime()));
                    NBTTagUtil.setData(place, "guild", GuildaDb.getGuildaP(uuid));
                    NBTTagUtil.setData(place, "type", NBTTagUtil.getStringData(player.getItemInHand(), "type"));
                }
            }
        }
    }
}
