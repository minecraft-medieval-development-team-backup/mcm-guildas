package com.mcm.guildas.listeners;

import com.mcm.core.database.GuildaDb;
import com.mcm.guildas.hashs.Plots;
import com.mcm.guildas.managers.*;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PlayerInteractNpc implements Listener {

    @EventHandler
    public void onClickNpc(NPCRightClickEvent event) {
        Player player = event.getClicker();
        String uuid = player.getUniqueId().toString();

        if (event.getNPC().getId() == Plots.get(GuildaDb.getGuildaP(uuid)).getNpc_instrutor())
            player.openInventory(InventoryInstructor.get(uuid));
        if (event.getNPC().getId() == Plots.get(GuildaDb.getGuildaP(uuid)).getNpc_mercador())
            player.openInventory(InventoryShop.get(uuid));
        if (event.getNPC().getId() == Plots.get(GuildaDb.getGuildaP(uuid)).getNpc_evolucao())
            player.openInventory(InventoryUpgrade.get(uuid));
        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
    }
}
