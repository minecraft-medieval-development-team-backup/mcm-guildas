package com.mcm.guildas.listeners;

import com.mcm.core.Main;
import com.mcm.core.database.GuildaDb;
import com.mcm.core.enums.GuildsServers;
import com.mcm.guildas.hashs.Plots;
import com.mcm.guildas.utils.PlotUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinEvents implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        /**
         * Here i'm setting the guild of player on cache
         */
        GuildaDb.getGuildaP(uuid);

        if (Main.server_name != null) for (GuildsServers server : GuildsServers.values()) {
            if (server.name().replaceAll("_", "-").equals(Main.server_name)) {
                if (Plots.get(GuildaDb.getGuildaP(uuid)) != null) {
                    player.teleport(PlotUtil.spiralCoords(Bukkit.getWorld("world")).get(Plots.get(GuildaDb.getGuildaP(uuid)).getPlot() - 1));
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);
                } else {
                    new Plots(GuildaDb.getGuildaP(uuid), uuid).insert();
                    GuildaDb.updateAlocated(GuildaDb.getGuildaP(uuid), Main.server_name);
                    player.teleport(PlotUtil.spiralCoords(Bukkit.getWorld("world")).get(Plots.get(GuildaDb.getGuildaP(uuid)).getPlot() - 1));
                    player.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0f, 1.0f);
                    player.sendTitle(ChatColor.YELLOW + GuildaDb.getGuildaP(uuid), ChatColor.GRAY + "Bem vindo!", 5, 40, 5);
                }
            }
        }
    }
}
