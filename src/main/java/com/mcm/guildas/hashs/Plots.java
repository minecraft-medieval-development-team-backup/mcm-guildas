package com.mcm.guildas.hashs;

import com.mcm.core.database.GuildaDb;
import com.mcm.core.utils.WorldEditUtil;
import com.mcm.guildas.Main;
import com.mcm.guildas.managers.NPCSPlot;
import com.mcm.guildas.utils.PlotUtil;
import com.sk89q.worldedit.WorldEditException;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Plots {

    public static ArrayList<Integer> plots;
    public static ArrayList<NPC> npcs;

    private String guild;
    private int plot;
    private Location location;
    private int npc_instrutor;
    private int npc_mercador;
    private int npc_evolucao;
    private String onNpcEvolucaoArea;
    private static HashMap<String, Plots> cache = new HashMap<>();

    public Plots(String guild, String uuid) {
        this.guild = guild;
        int plotFree = 1;
        if (plots != null) for (int i = 1; i <= 100; i++) {
            if (!plots.contains(i)) {
                plotFree = i;
                break;
            }
        }
        this.plot = plotFree;

        /**
         * Here i'm loading the plot, and placing him on this server files
         */
        GuildaDb.loadPlotSchem(guild);

        try {
            /**
             * TODO * Confirm if this local is correctly
             */
            Location location = PlotUtil.spiralCoords(Bukkit.getWorld("world")).get(plotFree - 1);
            this.location = location;

            if (WorldEditUtil.load("guild-plots/" + guild) != null) {
                WorldEditUtil.paste(WorldEditUtil.load("guild-plots/" + guild), true, location);
            }
            ArmorStand stand = location.getWorld().spawn(new Location(location.getWorld(), location.getBlockX(), 55, location.getBlockZ()), ArmorStand.class);
            stand.setCustomName("plot-info:" + guild);
            stand.setVisible(false);
            stand.setGravity(false);
            stand.setCustomNameVisible(false);

            Bukkit.getScheduler().runTaskLater(Main.plugin, () ->{
                NPCSPlot.enable(location, guild, uuid);
            }, 20L);
        } catch (WorldEditException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Plots insert() {
        if (plots == null) plots = new ArrayList<>();
        plots.add(this.plot);
        this.cache.put(this.guild, this);
        return this;
    }

    public static Plots get(String guild) {
        return cache.get(guild);
    }

    /**
     * Removing here of the plot/guild of the cache and all npcs of the plot if him exists.
     */
    public void delete() {
        /**
         * TODO * Replace the Y when confirm the real number of that on plot
         */
        try {
            WorldEditUtil.savePlot("guild-plots/" + this.guild, WorldEditUtil.copy(WorldEditUtil.getRegion(new Location(this.location.getWorld(), this.location.getBlockX() - 50, 63, this.location.getBlockZ() - 50), new Location(this.location.getWorld(), this.location.getBlockX() + 50, 113, this.location.getBlockZ() + 50))));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WorldEditException e) {
            e.printStackTrace();
        }

        NPCSPlot.disable(this.guild);
        plots.remove(this.plot);
        cache.remove(this.guild);
    }

    public int getPlot() {
        return this.plot;
    }

    public int getNpc_instrutor() {
        return npc_instrutor;
    }

    public void setNpc_instrutor(int npc_instrutor) {
        this.npc_instrutor = npc_instrutor;
    }

    public int getNpc_mercador() {
        return npc_mercador;
    }

    public void setNpc_mercador(int npc_mercador) {
        this.npc_mercador = npc_mercador;
    }

    public int getNpc_evolucao() {
        return npc_evolucao;
    }

    public void setNpc_evolucao(int npc_evolucao) {
        this.npc_evolucao = npc_evolucao;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String onNpcEvolucaoArea() {
        return onNpcEvolucaoArea;
    }

    public void setOnNpcEvolucaoArea(String onNpcEvolucaoArea) {
        this.onNpcEvolucaoArea = onNpcEvolucaoArea;
    }
}
