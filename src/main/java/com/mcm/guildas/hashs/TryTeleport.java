package com.mcm.guildas.hashs;

import scala.util.Try;

import java.util.HashMap;

public class TryTeleport {

    private String uuid;
    private static HashMap<String, TryTeleport> cache = new HashMap<>();

    public TryTeleport(String uuid) {
        this.uuid = uuid;
    }

    public TryTeleport insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static TryTeleport get(String uuid) {
        return cache.get(uuid);
    }

    public void delete() {
        this.cache.remove(this.uuid);
    }
}
