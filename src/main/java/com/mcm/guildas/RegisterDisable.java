package com.mcm.guildas;

import com.mcm.guildas.hashs.Plots;
import com.mcm.guildas.managers.NPCSPlot;
import net.citizensnpcs.api.npc.NPC;

public class RegisterDisable {

    public static void register() {
        if (Plots.npcs != null) for (NPC npc : Plots.npcs) {
            npc.destroy();
        }
        NPCSPlot.disableAllNpc();
    }
}
